///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Module principal du Back-end, initialisation et end-points.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

'use strict';

var wor                 = require('./server_words')   ;
var mon                 = require('./server_mongo')   ;
var pla                 = require('./server_players') ;
var gam                 = require('./server_games')   ;
var rea                 = require('./server_realtime');

// définitions pour le démarrage de nodejs express
var ip                  = require('ip'           );
var path                = require('path'         );
var express             = require('express'      );
var bodyParser          = require('body-parser'  );
var readlinesync        = require('readline-sync'); 
var app                 = express();
var http                = require('http').createServer(app);
var staticPath          = path.join(__dirname, '/');

// Definitions pour MongoDb et Mongoose 
var useMongoose         = true  ;

// mode de fonctionnement du serveur
var useSocketIo         = true  ; 

// variables spécifiques au serveur Pictionary
var nextPlayerId        = 1  ;

// couleurs pour les console.log()
var console_reset       = "\u001B[0m" ;
var console_red         = "\u001B[91m";
var console_green       = "\u001B[92m";
var console_yellow      = "\u001B[93m";
var console_cyan        = "\u001B[96m";
var console_white       = "\u001B[97m";

var initStep          = -1   ; // 0 configuration par défaut inchangée / -1 pour demander la configuration
var initProgressCount = 10  ; 
var initProgressIndex = 0   ;

// =================================== CoNFIGURATION DU SERVEUR ==========================================================
function configure_Server()
{
  initStep = 0 ;
  console.log(console_yellow) ;
  console.log('Entrez les 2 lettres (s ou S, suivi de m ou M) pour configurer le serveur,');
  var params = readlinesync.question('s:sans Socket.IO, S:avec Socket.IO, m:sans mongoose, M:avec mongoose : '  );
  console.log(console_reset) ;
  if (params.includes("S")) useSocketIo=true ;
  if (params.includes("s")) useSocketIo=false;
  if (params.includes("M")) useMongoose=true ;
  if (params.includes("m")) useMongoose=false;
}
// =================================== SERVER INIT AND ENPOINTS ==========================================================
if (initStep<0) configure_Server() ; // pour demander la configuration : "if (initStep==0)", sinon "if (initStep<0)"

init() ;

function init()
{
  initProgressIndex ++ ;
  if       (initStep==0)
  { // lancement de mon.mongoDb_Init
    mon.mongoDb_Init(1, useMongoose) ; initStep++ ; initProgressIndex=0 ; setTimeout(init,1000) 
  }
  else if  (initStep==1) 
  { // mon.mongoDb_Init en cours
    if (mon.mongoDb_IsInitOk()) 
    { // mon.mongoDb_Init Ok : suivant configurattion, lancement de mon.mongoose_Init ou de Server_Start
      if (useMongoose) { mon.mongoose_Init(2) ; initStep++ ; initProgressIndex=0 ; setTimeout(init,1000) }
      else server_Start() ;
    } 
    else 
    { // En cas de timeout, echec de l'init 
      if (initProgressIndex==initProgressCount) {  initStep=-1 ; console.log("l'initialisation de MongoDb a échoué. Serveur non démarré: Tapez sur CTRL-C pour quitter.") ;} 
      setTimeout(init,1000) ; 
    }
  }
  else if (initStep==2)
  { // mon.mongoose_Init en cours
    if (mon.mongoDb_mongooseInitialized())
    { // mon.mongoose_Init Ok : lancement de Server_Start
      initStep++ ; 
      server_Start() ; 
    }
    else 
    { // En cas de timeout, echec de l'init 
      if (initProgressIndex==initProgressCount) { initStep=-1 ; console.log("l'initialisation de mongoose a échoué. Serveur non démarré: Tapez sur CTRL-C pour quitter.") ; } 
      setTimeout(init,1000) ; 
    }
  }
  else if (initStep==-1) setTimeout(init,1000) ; // Echec de la procédure d'init : continuer pour éviter la fin de programme, afin de voir la console.
}

function server_Start()
{
// demarrage du serveur
app.use(express.static(staticPath));
app.use(bodyParser.json({ limit: "50mb" })) ;
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 })) ;
app.set('port', process.env.PORT || 3000);
var srv = http.listen(app.get('port'), function() { console.log("Seveur à l'écoute : "+ ip.address() + " port : " + http.address().port)});

rea.refreshActions_init(useSocketIo, http) ; 

function console_log(endpoint,sin,sout,error)
{
  var msg = console_green+endpoint.padEnd(20)+console_reset ;
  if (sin  !="" ) msg+=" (IN) " +sin ;
  if (sout !="" ) msg+=" (OUT) "+sout;
  if (error!="" ) msg+=(sout ==""?" (OUT)":"")+" error:"+console_red+error+console_reset ;
  console.log(msg)
}

// ================================== /playerId (GET) =================================================================
// (IN) (OUT) playerId, playerName, socketIo
app.get ('/playerId', function(req, res) {
     console_log("/playerId", "", "playerId:"+nextPlayerId+" playerName:"+"Joueur#"+nextPlayerId,"") ;
     rea.refreshActions_AddItem(nextPlayerId) ;
     return res.json({playerName:"Joueur#"+nextPlayerId,playerId:nextPlayerId++, socketIo : useSocketIo});	
    });

// ================================== /connect (POST)=================================================================
// (IN) playerId,playerName, password (OUT) role, error
app.post('/connect', async function(req, res) 
{ 
    var result = { role:"N", error:"" }
    await pla.connect(req.body.playerId,req.body.playerName,req.body.password).then((res)=> result = res ) ;
    console_log("/connect", "playerId:"+req.body.playerId+" playerName:"+req.body.playerName+" password:***", " role:"+result.role, result.error); 
    return res.json(result);
} );

// ================================== /register =================================================================
// (IN) playerName, password (OUT) error
app.post('/register', async function(req, res) 
{ 
    var theError = "" ;
    await pla.register(req.body.playerName,req.body.password).then((result)=>theError = result) ;
    console_log("/register","playerName:"+req.body.playerName+" password:***", "", theError); 
    return res.json( {error:theError });
} ); 

// ================================== /rights (POST) =================================================================
// (IN) playerName,role (OUT) error     //  role="A"(Admin) / "C"(utilisateur lambda connecté) /"" (supprimer le compte)  
app.post('/rights', async function(req, res) 
{ 
    var theError = "" ;
    await pla.setRights(req.body.playerName,req.body.role).then((result)=>theError = result) ;
    console_log("/rights", "playerName:"+req.body.playerName+" role:"+req.body.role, "", theError); 
    return res.json( {error:theError });
} ); 

// ================================== /disconnect (POST) =================================================================
// (IN) playerId (OUT) playerName

app.post('/disconnect', function(req, res) 
{ 
    var playerId  = req.body.playerId ;
    console_log("/disconnect", "playerId:"+playerId, "", "");
    return res.json({ playerName:"Joueur#"+playerId }); // retourne le nom de joueur anonyme basé sur la valeur de playerId 
} );

// ================================== /exit (PUT) =================================================================
// (IN) playerId 
app.put('/exit', function(req) 
{ 
    var playerId  = req.body.playerId  ;
    rea.RefreshAction_RemoveItem(playerId) ;
    gam.exitGames(playerId) ;
    console_log("/exit", "playerId="+playerId, "", "") ;
} );

// ================================== /refresh (GET) =================================================================
// (IN) playerId (OUT) refresActions[]

app.post('/refresh', function(req, res) 
{ 
    var item = rea.refreshActions_Get(req.body.playerId) ;
    // pas de console.log, sauf debug 
    if (item!=null && item==null) console_log("/refresh", "playerId:"+req.body.playerId, "refreshActions:"+
          (item.game==null?"":" game")+(item.imageData==null?"":" imageData")+(gam.allGames()==null?"null":" games.length="+gam.allGames().length), "" );   
    var result = item==null ? res.json(null) : res.json({refreshActions:item}) ;
    if (item!=null) rea.refreshActions_ClearItem(item) ;
    return result ;
} );

// ================================== /createGame (POST)=================================================================
// (IN) playerId, playerName, roundsCount (OUT) game, word, error
app.post('/createGame', async function(req, res) 
{ 
  var gameAndWord = { game:null, word:"", error:"" };
  await gam.createGame(req.body.playerId,req.body.playerName,req.body.roundsCount).then((result)=>gameAndWord = result) ;
    console_log("/createGame", "playerId:"+req.body.playerId+" playerName:"+req.body.playerName+" roundsCount:"+req.body.roundsCount, "gameId:"+gameAndWord.game.gameId, gameAndWord.error) ;
    return res.json({game:gameAndWord.game, word : gameAndWord.word, error : gameAndWord.error});
} );

// ================================== /getGames (GET) =================================================================
// (IN) started (OUT) games[]  <started=false(jeux en attente/true(jeux encours) < games : array of game > 
app.get('/getGames', function(req, res) 
{ 
    var theGames = gam.getGames() ;
    console_log("/getGames", "", "games.length:"+theGames.length,"") ;
    return res.json({games:theGames});
} );

// ================================== /joinGame (POST) =================================================================
// (IN) gameId,playerId,playerName (OUT) game,error
app.post('/joinGame', function(req, res) 
{ 
    var result  = gam.joinGame(req.body.gameId, req.body.playerId, req.body.playerName) ;
    console_log("/joinGame", "gameId:"+req.body.gameId +"  playerId:"+req.body.playerId + "  playerName:"+req.body.playerName, "" , "") ;
    return res.json(result);
} );

// ================================== /startGame (POST) =================================================================
// (IN) gameId (OUT) word,error
app.post('/startGame', function(req, res) 
{ 
    var wordAndError  = gam.startGame(req.body.gameId) ;
    console_log("/startGame", "gameId:"+req.body.gameId, "word:"+wordAndError.word, wordAndError.error) ;
    return res.json({word : wordAndError.word, error : wordAndError.error});
} );

// ================================== /newImage (POST) =================================================================
// (IN) gameId,imageData (OUT) error

app.post('/newImage', function(req, res) 
{ 
    var theError  = gam.newImage(req.body.gameId,req.body.imageData) ;
    console_log("/newImage", "gameId:"+req.body.gameId, "", theError) ;
    return res.json({error : theError});
} );

// ================================== /stopRound (POST) =================================================================
// (IN) gameId (OUT) word,error
app.post('/stopRound', function(req, res) 
{ 
    var result  = gam.stopRound(req.body.gameId,false) ;
    console_log("/stopRound","gameId:" + req.body.gameId, "", result.error) ;
    return res.json(result);
} );

// ================================== /propose (POST) =================================================================
// (IN) gameId,playerId,word (OUT) game,success,error
app.post('/propose', function(req, res) 
{ 
    var result  = gam.propose(req.body.gameId,req.body.playerId,req.body.word) ;
    console_log("/propose", "gameId:" + req.body.gameId+" playerId:"+req.body.playerId+" word:"+req.body.word,"success:" +result.success, "") ;
    return res.json(result);
} );

// ================================== /leaveGame (POST) =================================================================
// (IN) gameId,playerId (OUT) error 

app.post('/leaveGame', function(req,res) 
{ 
    console_log("/leaveGame","gameId:" + req.body.gameId+" playerId:"+req.body.playerId, "", "") ;
    gam.leaveGame(req.body.gameId,req.body.playerId) ;
    return  res.json({ error:"" }) ;
} );


// ================================== /stopGame (POST) =================================================================
// (IN) gameId (OUT) error
app.post('/stopGame', function(req,res) 
{ 
  var theError = gam.stopGame(req.body.gameId,req.body.playerId) ;
  console_log("/stopGame", "gameId:" + req.body.gameId, "",theError) ;
  return  res.json({ error:theError }) ;
} );

// ================================== /getNewWord (POST) =================================================================
// getNewWord (IN) gameId (OUT) word,error 
app.post('/getNewWord', function(req, res)
{
  var result = gam.getNewWord(req.body.gameId) ;
  console_log("/getNewWord", "gameId:" + req.body.gameId, "word:" + result.word,result.error) ;
  return  res.json(result) ;
} );

// ================================== /getWords (GET) =================================================================
// (IN) (OUT) words[],error
app.get('/getWords', async function(req, res)
{
  var theResult = [] ;
  await wor.getWords().then((result)=>theResult = result) ;
  console_log("getWords", "","words: " + theResult.words.toString().substr(0, 60), "") ;
  return res.json(theResult) ;
})

// ================================== /setWords (POST) =================================================================
// (IN) oldWords[],newWords[] (OUT) error
app.post('/setWords', async function(req, res)
{
  var theError = {} ;
  await wor.setWords(req.body.oldWords, req.body.newWords).then((error)=>theError = error) ;
  console_log("setWords", "oldWords: " + req.body.oldWords.toString().substr(0, 60)+" newWords: " + req.body.newWords.toString().substr(0, 60), "" , "") ;
  return res.json({error : theError})
})

// ================================== /getGamesHistory (POST)=================================================================
// (IN) playerName (OUT) games[],error  <playerName="" pour tous les joueurs> < games : array of game >
app.post('/getGamesHistory', async function(req, res) 
{ 
    var theResult = {} ;
    await gam.getGamesHistory(req.body.playerName).then((result)=>theResult = result) ;
    console_log("/getGamesHistory", (req.body.playerName==""? "<all>":req.body.playerName), "games.length:"+ (theResult.games==null? 0:theResult.games.length), theResult.error) ;
    return res.json({games: theResult.games, error : theResult.error});
} );

// ================================== /deleteGameHistory (POST)=================================================================
// (IN) dbGameId (OUT) error    <dbGameId=-1 pour toutes les parties>
app.post('/deleteGameHistory', async function(req, res) 
{ 
    var theError = {} ;
    await gam.deleteGameHistory(req.body.dbGameId).then((error)=>theError = error) ;
    console_log("/deleteGameHistory", (req.body.dbGameId<0 ? "<all>" : "<one>"), "", theError) ;
    return res.json({error : theError});
} );

} // serverInit