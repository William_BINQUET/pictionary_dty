///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Méthodes communes appelées par les composants React :
// - set_Props(divComponent,props)
// - change_Display(divComponent,vdisplay)  
// - gameToString(game,format)
// - fetchOk(divComponent,json)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function set_Props(divComponent,props) 
{ 
    divComponent.changeDisplay = divComponent.changeDisplay.bind(divComponent) ; 
    divComponent.state = {display : divComponent.props.display, updated:false, changeDisplayFunction: divComponent.changeDisplay, actionProcessFunction : props.actionProcessFunction } 
}

export function change_Display(divComponent,vdisplay)  
{ 
  var doRender = divComponent.state.display!=vdisplay || (vdisplay && divComponent.state.updated) ;
  // divComponent.setState({display : vdisplay}) ; 
  // if (doRender) {  divComponent.state.updated = false ; divComponent.forceUpdate() ; }
  divComponent.state.display=vdisplay ; 
  if (doRender) divComponent.state.updated = false ; 
  divComponent.forceUpdate()
}

export function gameToString(game,format) // (OUT) string
{ // format = JOIN,ACTIVE,CREATOR
    var commonHeader = "Partie créée le "+formatDate(new Date(game.dateTime))+" par "+game.creatorName+" ("+game.roundsCount+" manche"+(game.roundsCount==1?"":"s")+", "+ 
      (game.terminated? (game.roundIndex<game.roundsCount?"partie annulée manche "+game.roundIndex      : "partie terminée"  ):
                        (game .started                   ?"manche "+(game.roundIndex+1)+" en cours"     : "partie en attente")  )+")\n" ;
    var wordsLine    = "" ;
    var playersLine  = "" ;
    if (format!="JOIN") { for (var i=0;i<Math.min(game.roundIndex, game.roundsCount);i++) wordsLine+=(i+1)+":"+game.words[i]+(game.finders[i]==null?"":"("+game.finders[i].name+")") +" " ; wordsLine+="\n" ; }
    for (var j=0;j<game.players.length;j++) playersLine+=game.players[j].name+(format=="JOIN"?"":" : "+game.players[j].score+(game.players[j].gone?"(abandon)":"")  )+" " ;
    return commonHeader+wordsLine+playersLine ;
}

export function fetchOk(divComponent,json)
{
  var result = false ;
  if (json==null) json={error:"Pas de réponse du serveur"};
  if (json.error!=null && json.error!="") divComponent.state.actionProcessFunction({actionName:"displayError",errorText:json.error} ) ;
  else result = true ;
  return result ;
}

function formatDate(d) { return pad2(d.getDate())+'/'+ pad2(d.getMonth()) + '/' +d.getFullYear() + ' ' + pad2(d.getHours()) + ':' + pad2(d.getMinutes()) ; }

function pad2(n) { return n<10 ? "0"+n:""+n}