﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constantes, variables et méthodes partagées par tous les modules.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

// -------------- Options du menu ----------------
export const menu_TitreConnexion          = "<CONNEXION>"                  ;
export const menu_TitreJeu                = "<JEU>"                        ;
export const menu_TitreAdministration     = "<ADMINISTRATION>"             ;
export const menu_MeConnecter             = "Me connecter"                 ;
export const menu_CreerCompte             = "Créer un compte"              ;
export const menu_MeDeconnecter           = "Me déconnecter"               ;
export const menu_CreerPartie             = "Créer une partie"             ;
export const menu_RejoindrePartie         = "Rejoindre une partie"         ;
export const menu_HistPartiesCreees       = "Historique des parties créées";
export const menu_Jouer                   = "Jouer"                        ;
export const menu_JouerCreateur           = "Jouer(créateur)"              ;
export const menu_PartiesEnCours          = "Parties en cours"             ;
export const menu_HistParties             = "Historique des parties"       ;
export const menu_GestionDroits           = "Gestion des droits"           ;
export const menu_ReferentielMots         = "Référentiel de mots"          ;

export const div_MeConnecter             = "MeConnecter"       ;      
export const div_MeDeconnecter           = "MeDeconnecter"     ;      
export const div_CreerCompte             = "CreerCompte"       ;      
export const div_CreerPartie             = "CreerPartie"       ;      
export const div_RejoindrePartie         = "RejoindrePartie"   ;  
export const div_Jouer                   = "Jouer"             ;  
export const div_JouerCreateur           = "JouerCreateur"     ;  
export const div_HistPartiesCreees       = "HistPartiesCreees" ;
export const div_PartiesEnCours          = "PartiesEnCours"    ;
export const div_HistParties             = "HistParties"       ;      
export const div_GestionDroits           = "GestionDroits"     ;    
export const div_ReferentielMots         = "ReferentielMots"   ;  

export const game_Attente                = "Attente"           ;
export const game_Jouant                 = "Jouant"            ;
export const game_JouantCreateur         = "JouantCreateur"    ;

export const picture_Heigth              = "300"               ;
export const picture_Width               = "400"               ;
export const picture_Style               = {border:"1px solid black"};
export const picture_DisabledStyle       = {border:"1px dotted red" };

// -------------- Roles des utilisateurs ----------------
export const role_NonConnecte            = "N";
export const role_Connecte               = "C";
export const role_Admin                  = "A";

// -------------- Variable globales ----------------
export var configParams                  = { useSocketIO : false} ;
export var gPlayer                       = { playerId:0, playerName:"Joueur0", role:role_NonConnecte, gameStatus:game_Attente } ;

// -------------- Outils Divers ------------------------------
export function removeAccents(str) {
    var accents    = 'ÀÁÂÃÄÅàáâãäåßÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = "AAAAAAaaaaaaBOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    str = str.split('');
    var strLen = str.length;
    var i, x;
    for (i = 0; i < strLen; i++) {
      if ((x = accents.indexOf(str[i])) != -1) {
        str[i] = accentsOut[x];
      }
    }
    return str.join('');
  }

export function doNothing(){return true}