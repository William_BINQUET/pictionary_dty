///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Module de définition des fonctions de bases de mongo_Db/mongoose (ajout, suppression, connexion à la base de données).
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

'use strict';

var ObjectId            = require('mongodb').ObjectID;

module.exports = {mongoDb_getObjectId, mongoDb_IsInitOk, mongoDb_findOne, mongoDb_deleteOne, mongoDb_insertOne, mongoDb_Init, mongoose_Init, mongoDb_deleteMany, mongoDb_findItems, mongoDb_update, mongoose_getDbItem, mongoDb_mongooseInitialized}

// Definitions pour MongoDb et Mongoose 
var mongoDbConnection   = require('mongodb').MongoClient;
var mongoDbName         = "pictionnaryDb" ; 
var mongoUrl            = "mongodb://localhost:27017/" + mongoDbName;
var mongoose            = require('mongoose');
var mongooseUsersModel  = null ;
var mongooseGamesModel  = null ;
var mongooseWordsModel  = null ;

// mode de fonctionnement du serveur
var useMongoose         = true  ; 

function doNothing(){}

//================================ fonctions generales mongodb/mongoose ====================================== 

var mongoDb_InitOk = [ false, false, false ]  ;

async function mongoDb_Init(initStep,vUseMongoose) { 
  useMongoose = vUseMongoose ;
  try
  {
    await mongoDbConnection.connect(mongoUrl, {useUnifiedTopology : true}, function(err, db) 
    {
      var dbo = db.db(mongoDbName);
      dbo.createCollection("wordsCollection", function(err, res) {doNothing(res) ; if (initStep==1) console.log("mongodb: wordsCollection "+(err?err:"Ok")); mongoDb_InitOk[0]=!err ; db.close();});
      dbo.createCollection("gamesCollection", function(err, res) {doNothing(res) ; if (initStep==1) console.log("mongodb: gamesCollection "+(err?err:"Ok")); mongoDb_InitOk[1]=!err ; db.close();});
      dbo.createCollection("usersCollection", function(err, res) {doNothing(res) ; if (initStep==1) console.log("mongodb: usersCollection "+(err?err:"Ok")); mongoDb_InitOk[2]=!err ; db.close();});
    })
  }
  catch { console.log('Erreur de connection MongoDb'); }
}

function mongoDb_IsInitOk() {return mongoDb_InitOk[0] && mongoDb_InitOk[1] && mongoDb_InitOk[2]}

async function mongoose_Init(initStep) { 
  try {
    mongoose.connect(mongoUrl, {useUnifiedTopology : true, useNewUrlParser : true}) ;
    mongoose.connection.on  ('error', function (err) { console.log('Erreur de connection Mongoose ' + err )});
    mongoose.connection.once('connected' , function () { 
      mongooseUsersModel = mongoose.model('usersCollection', new mongoose.Schema({ }, {collection : "usersCollection", strict:false})) ;
      mongooseWordsModel = mongoose.model('wordsCollection', new mongoose.Schema({ }, {collection : "wordsCollection", strict:false})) ;
      mongooseGamesModel = mongoose.model('gamesCollection', new mongoose.Schema({ }, {collection : "gamesCollection", strict:false})) ;
      if (initStep==2) console.log('mongoose: connexion  OK') ;
      mongoose.set('useFindAndModify', false);
    });          
  } 
catch { if (initStep==2) console.log('mongoose: erreur de connexion'); }
}

function mongoose_getModel(collectionName) // (OUT) dbItem
{
  var result = null ;
  switch (collectionName)
  {
    case "usersCollection" : result = mongooseUsersModel ; break ;
    case "wordsCollection" : result = mongooseWordsModel ; break ;
    case "gamesCollection" : result = mongooseGamesModel ; break ;
  }
  return result ;
}

function mongoose_getDbItem(collectionName,item) // (OUT) dbItem
{
  return new (mongoose_getModel(collectionName))(item) ;
}

async function mongoDb_Connect() // (OUT) db, dbo, error 
{
  var db         = await mongoDbConnection.connect(mongoUrl, {useUnifiedTopology : true}) ;
  var err        = db!=null?"": "erreur de connection à MongoDb: " + mongoUrl ;
  var dbo        = null ;
  if (db!=null) 
  { 
    dbo = db==null?null:db.db(mongoDbName) ;
    if (dbo==null) err = "erreur de connection à MongoDb: " + mongoDbName ;
  }
  return{db: db, dbo: dbo, error: err}
}

async function mongoDb_deleteOne(collectionName,filter) // (OUT) error
{
  var theError = "" ;
  if (useMongoose)  
  { // ----------------------- Mongoose ----------------------------------
    var model = mongoose_getModel(collectionName) ;
    await model.deleteOne(filter)
    .then  (function (err) { doNothing(err)}) 
    .catch (function (err) { theError = "la suppression dans MongoDb "+collectionName+" a échoué. Error:"+err ; }) ;
  }
  else 
  { // ----------------------- Mongodb ----------------------------------
    var connect = {};
    await mongoDb_Connect().then((result)=>{connect=result}) ;
    if (connect.error!="") theError=connect.error ;
    else
    {
      await connect.dbo.collection(collectionName).deleteOne(filter)
      .catch (function (err) { theError = "la suppression dans MongoDb "+collectionName+" a échoué. Error:"+err ; }) ;
      connect.db.close();
    } 
  }
  return { error : theError }
} 

async function mongoDb_deleteMany(collectionName,filter) // (OUT) error
{
  var theError = "" ;
  if (useMongoose)  
  { // ----------------------- Mongoose ----------------------------------
    var model = mongoose_getModel(collectionName) ;
    await model.deleteMany(filter)
    .then  (function (err) { doNothing(err)}) 
    .catch (function (err) { theError = "la suppression dans MongoDb "+collectionName+" a échoué. Error:"+err ; }) ;
  }
  else 
  { // ----------------------- Mongodb ----------------------------------
    var connect = {};
    await mongoDb_Connect().then((result)=>{connect=result}) ;
    if (connect.error!="") theError=connect.error ;
    else
    {
      await connect.dbo.collection(collectionName).deleteMany(filter)
      .catch (function (err) { theError = "la suppression dans MongoDb "+collectionName+" a échoué. Error:"+err ; }) ;
      connect.db.close();
    } 
  }
  return { error : theError }
} 

async function mongoDb_findItems(collectionName,filter) // (OUT) item, error
{
  var theError = ""  ;
  var theItems = []  ; 
  if (useMongoose)  
  { // ----------------------- Mongoose ----------------------------------
    var model = mongoose_getModel(collectionName) ;
    if (model==null) theError = "L'initialisation de Mongoose est en cours ou a échoué." ;
    else await model.find(filter)
    .then  (function(items) { if (items!=null) for (var i=0; i<items.length; i++) theItems.push(items[i]._doc) ; })
    .catch ((err     )=>{ theError = "la recherche dans MongoDb "+collectionName+" a échoué. Error:"+err ; }) ;
  }
  else 
  { // ----------------------- Mongodb ----------------------------------
    var connect = {};
    await mongoDb_Connect().then((result)=>{connect=result}) ;
    if (connect.error!="") theError=connect.error ;
    else 
    {
      theItems = await connect.dbo.collection(collectionName).find(filter).toArray() ;
      connect.db.close();
    } 
  }
  return {items: theItems, error : theError }; 
} 

async function mongoDb_findOne(collectionName,filter,itemIsArray) // (OUT) item, error
{
  var theError = ""  ;
  var theItem  = null; 
  if (useMongoose)  
  { // ----------------------- Mongoose ----------------------------------
    var model = mongoose_getModel(collectionName) ;
    if (model==null) theError = "L'initialisation de Mongoose est en cours ou a échoué." ;
    else await model.findOne(filter)
    .then  (function(item) { if (item!=null) theItem = item._doc ; })
    .catch ((err     )=>{ theError = "l'insertion dans MongoDb "+collectionName+" a échoué. Error:"+err ; }) ;
  }
  else 
  { // ----------------------- Mongodb ----------------------------------
    var connect = {};
    await mongoDb_Connect().then((result)=>{connect=result}) ;
    if (connect.error!="") theError=connect.error ;
    else 
    {
      theItem = await connect.dbo.collection(collectionName).findOne(filter) ;
      connect.db.close();
    } 
  }
  if (theItem!=null && itemIsArray) theItem=theItem.itemArray ; 
  return {item:theItem, error : theError }; 
} 

async function mongoDb_insertOne(collectionName,item,itemIsArray) // (OUT) error
{
  var theError = "" ;
  if (useMongoose)  
  { // ----------------------- Mongoose ----------------------------------
    var dbItem = mongoose_getDbItem(collectionName,itemIsArray?{itemArray: item}:item) ;
    await dbItem.save()
    .then  (function (){ }) 
    .catch (function (err) { theError = "l'insertion dans MongoDb "+collectionName+" a échoué. Error:"+err ; }) ;
  }
  else 
  { // ----------------------- Mongodb ----------------------------------
    var connect  = {};
    await mongoDb_Connect().then((result)=>{connect=result }) ;
    if (connect.error!="") theError = connect.error ;
    else 
    {
      await connect.dbo.collection(collectionName).insertOne(itemIsArray?{itemArray: item}:item,{writeConcern: item}) 
      .then  (function (){ }) 
      .catch (function (err) { theError = "l'insertion dans MongoDb "+collectionName+" a échoué. Error:"+err ; }) ;
      connect.db.close();
    }
  } 
  return { error : theError }; 
  }

async function mongoDb_update(collectionName,filter,item,itemIsArray) // (OUT) error
  {
    var theError = "" ;
    if (useMongoose)  
    { // ----------------------- Mongoose ----------------------------------
      var model = mongoose_getModel(collectionName) ;
      await model.findOneAndReplace(filter, itemIsArray?{itemArray: item}:item, {}, doNothing) 
      .then  (function (){ })
      .catch (function(err    ) {                            theError = "la modification dans MongoDb "+collectionName+" a échoué. Error:"+err}) ;
    }
    else 
    { // ----------------------- Mongodb ----------------------------------
      var connect  = {};
      await mongoDb_Connect().then((result)=>{connect=result }) ;
      if (connect.error!="") theError = connect.error ;
      {
        await connect.dbo.collection(collectionName).update(filter,itemIsArray?{itemArray: item}:item,{writeConcern: item})
        .then  (function (writeResult){ if (writeResult.hasWriteConcernError()) theError = "la modification dans MongoDb "+collectionName+" a échoué. Error:"+writeResult.writeConcernError.errmsg})  
        .catch (function (err) { "la modification dans MongoDb "+collectionName+" a échoué. Error:"+err}) ;
        connect.db.close();
      } 
    }
    return {error : theError}; 
  }

function mongoDb_mongooseInitialized() {return mongooseUsersModel!=null && mongooseGamesModel!= null  && mongooseWordsModel!= null}

function mongoDb_getObjectId(id) {return ObjectId(id)}