﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Composant React pour l'affichage des erreurs :
// - ErrorComponent  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

import * as genprocs  from './appcomponents_genprocs' ;

declare var require: any
var React    = require('react');

export class ErrorComponent extends React.Component {
    constructor (props) { 
      super (props) ; 
      genprocs.set_Props(this,props) ; 
      this.displayError               = this.displayError.bind(this) ; 
      this.state.displayErrorFunction = this.displayError ;
      this.state.error                = "" ;
    }
    
    render() {  
      var error                 = this.state.error.startsWith("!")?this.state.error.substring(1) :this.state.error  ;
      var errorButtonclass      = "button is-dark is-small "+(this.state.error.startsWith("!")?"is-success"      :"is-danger"      ) ;
      var errorLabelclass       = "label "                  +(this.state.error.startsWith("!")?"has-text-success":"has-text-danger") ;
      return ((this.state.error=="") ||
      <div class="field has-addons" id="errorDiv" >
         <div class="control"> <button class={errorButtonclass} type="submit" onClick={() => {this.displayError("")}}>X</button> </div>
         <div class="control"> <label class="label" >&nbsp;&nbsp;</label>                                            </div>
         <div class="control"> <label class={errorLabelclass} id="errorText">{error}</label>                         </div>
      </div>
      ) }
    
      displayError(error) {this.state.error = error ; this.forceUpdate() ; }

      changeDisplay() {}
 } // ErrorComponent ================================================================