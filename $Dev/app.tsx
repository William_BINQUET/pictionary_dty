﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Module principal, création du composant React principal : 
// - PictionaryComponent
// Communication entre les modules secondaires.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

import { MenuGeneralComponent        } from './menugeneral'          ;
import { MeConnecterComponent        } from './appcomponents_players';
import { MeDeconnecterComponent      } from './appcomponents_players';
import { CreerCompteComponent        } from './appcomponents_players';
import { CreerPartieComponent        } from './appcomponents_games'  ;
import { RejoindrePartieComponent    } from './appcomponents_games'  ;
import { JouerComponent              } from './appcomponents_games'  ;
import { JouerCreateurComponent      } from './appcomponents_games'  ;
import { HistPartiesCreeesComponent  } from './appcomponents_games'  ;
import { PartiesEnCoursComponent     } from './appcomponents_games'  ;
import { HistPartiesComponent        } from './appcomponents_games'  ;
import { GestionDroitsComponent      } from './appcomponents_players';
import { ReferentielMotsComponent    } from './appcomponents_words'  ;
import { ErrorComponent              } from './appcomponents_errors' ;

import * as commons from './commons';

declare var require: any
var React    = require('react');
var ReactDOM = require('react-dom') ;

var socketIo = require('socket.io-client') ;
var mainComponent = null                   ;

//supression des ESLint error 
commons.doNothing(MenuGeneralComponent, MeConnecterComponent, MeDeconnecterComponent, CreerCompteComponent, CreerPartieComponent, RejoindrePartieComponent, JouerComponent, JouerCreateurComponent, HistPartiesCreeesComponent, PartiesEnCoursComponent, HistPartiesComponent, GestionDroitsComponent, ReferentielMotsComponent, ErrorComponent) ;

export class PictionaryComponent extends React.Component {
    constructor (props) 
    { 
        super (props) ; 
        this.menuEvent                      = this.menuEvent     .bind(this);
        this.actionProcess                  = this.actionProcess .bind(this);
        this.displayError                   = this.displayError  .bind(this);
        this.processRefresh                 = this.processRefresh.bind(this);
        this.socketInit                     = this.socketInit    .bind(this);
        this.executeRefresh                 = this.executeRefresh.bind(this);
        this.selectedComponent              = null; 
        this.ref_MenuGeneralComponent       =  React.createRef() ; 
        this.ref_MeConnecterComponent       =  React.createRef() ;
        this.ref_MeDeconnecterComponent     =  React.createRef() ;
        this.ref_CreerCompteComponent       =  React.createRef() ;
        this.ref_CreerPartieComponent       =  React.createRef() ;
        this.ref_RejoindrePartieComponent   =  React.createRef() ;
        this.ref_JouerComponent             =  React.createRef() ;
        this.ref_JouerCreateurComponent     =  React.createRef() ;
        this.ref_HistPartiesCreeesComponent =  React.createRef() ;
        this.ref_PartiesEnCoursComponent    =  React.createRef() ;
        this.ref_HistPartiesComponent       =  React.createRef() ;
        this.ref_GestionDroitsComponent     =  React.createRef() ;
        this.ref_ReferentielMotsComponent   =  React.createRef() ;
        this.ref_ErrorComponent             =  React.createRef() ;
        this.state = { error : "" , socket : null, selectedMenuItem : commons.div_RejoindrePartie} ;
    }
    
    componentDidMount() { 
      this.state.count = 0 ;     
      if (!commons.configParams.useSocketIo) setInterval(this.processRefresh,1000) ; 
      this.displayError("") ; 
      this.getPlayerId()    ; 
      mainComponent = this  ; 
    }
    
    render() { 
         return (
            <div>
                <label class="label has-text-link">Pictionary &nbsp;<small>par William Binquet 202</small></label>
                <br/>
                <div class="columns is-fullheight">
                    <div id = "mainDiv" class="column is-2 is-sidebar-menu">
                        <MenuGeneralComponent ref={this.ref_MenuGeneralComponent } 
                           role={commons.role_NonConnecte} menuOption={commons.div_RejoindrePartie} gameStatus={commons.game_Attente} menuEvent={this.menuEvent} 
                        />
                        <br/><br/>&nbsp;&nbsp;&nbsp;
                        <label class="label has-text-primary" id="playerLabel">&nbsp;&nbsp;{commons.gPlayer.playerName}</label>
                    </div>

                    <div class="column is-main-content">
                       <div>
                          <ErrorComponent            ref={this.ref_ErrorComponent            } display={false} actionProcessFunction={this.actionProcess} />
                       </div>
                       <div>
                         <MeConnecterComponent       ref={this.ref_MeConnecterComponent      } display={false} actionProcessFunction={this.actionProcess} />
                         <MeDeconnecterComponent     ref={this.ref_MeDeconnecterComponent    } display={false} actionProcessFunction={this.actionProcess} />
                         <CreerCompteComponent       ref={this.ref_CreerCompteComponent      } display={false} actionProcessFunction={this.actionProcess} />
                         <CreerPartieComponent       ref={this.ref_CreerPartieComponent      } display={false} actionProcessFunction={this.actionProcess} />
                         <RejoindrePartieComponent   ref={this.ref_RejoindrePartieComponent  } display={true } actionProcessFunction={this.actionProcess} />
                         <JouerComponent             ref={this.ref_JouerComponent            } display={false} actionProcessFunction={this.actionProcess} />
                         <JouerCreateurComponent     ref={this.ref_JouerCreateurComponent    } display={false} actionProcessFunction={this.actionProcess} />
                         <HistPartiesCreeesComponent ref={this.ref_HistPartiesCreeesComponent} display={false} actionProcessFunction={this.actionProcess} />
                         <PartiesEnCoursComponent    ref={this.ref_PartiesEnCoursComponent   } display={false} actionProcessFunction={this.actionProcess} />
                         <HistPartiesComponent       ref={this.ref_HistPartiesComponent      } display={false} actionProcessFunction={this.actionProcess} />
                         <GestionDroitsComponent     ref={this.ref_GestionDroitsComponent    } display={false} actionProcessFunction={this.actionProcess} />
                         <ReferentielMotsComponent   ref={this.ref_ReferentielMotsComponent  } display={false} actionProcessFunction={this.actionProcess} />
                       </div>
                    </div>
               </div>
           </div>
        );
    }
  
    socketInit()
    {
      this.state.socket = socketIo.connect(window.location.origin);  
      this.state.socket.emit('init',   {playerId: commons.gPlayer.playerId});  
      this.state.socket.on  ('refresh', function(item){mainComponent.executeRefresh(item)})
    }


    getPlayerId() {
        fetch("/playerId")
        .then((res ) => { return res.json() })
        .then((json) => {              
          commons.gPlayer.playerId                = json.playerId   ; 
          commons.gPlayer.playerName              = json.playerName ;
          commons.configParams.useSocketIo        = json.socketIo   ;
          if (commons.configParams.useSocketIo) this.socketInit()   ;
          this.forceUpdate() ;
        })     
    }

    processRefresh()
    {
      fetch("/refresh", { method:  'POST',
         headers: {'Accept': 'application/json','Content-Type': 'application/json'},
         body:    JSON.stringify({ playerId : commons.gPlayer.playerId })
      } )
      .then((res ) => { return res.ok?res.json():null })
      .then((json) => { if (json!=null) 
        {
          this.executeRefresh(json.refreshActions)
        } 
      } ) ;
    }

    executeRefresh(refreshActions)
    {
      this.ref_RejoindrePartieComponent.current.state.refreshFunction(refreshActions) ;
      this.ref_JouerCreateurComponent  .current.state.refreshFunction(refreshActions) ;
      this.ref_JouerComponent          .current.state.refreshFunction(refreshActions) ;
      this.ref_RejoindrePartieComponent.current.state.refreshFunction(refreshActions) ;
    }

    actionProcess(action)
    {
        switch (action.actionName)
        {
          case "displayError" : 
              this.displayError(action.errorText) ; 
              break ;
          case "changeRole"   : 
             this.menuEvent(commons.div_RejoindrePartie) ; 
             this.ref_MenuGeneralComponent.current.state.changeSelectedMenuItemFunction(this.ref_MenuGeneralComponent.current, commons.div_RejoindrePartie) ;
             break ;
          case "changeGame"   : 
              var newSelectedMenuitem = "" ;
              switch(commons.gPlayer.gameStatus)
              {
                case commons.game_Attente        : newSelectedMenuitem = commons.div_RejoindrePartie ; break ;
                case commons.game_Jouant         : newSelectedMenuitem = commons.div_Jouer           ; break ;
                case commons.game_JouantCreateur : newSelectedMenuitem = commons.div_JouerCreateur   ; break ;
              }
                this.menuEvent(newSelectedMenuitem) ; 
                this.ref_MenuGeneralComponent.current.state.changeSelectedMenuItemFunction(this.ref_MenuGeneralComponent.current, newSelectedMenuitem) ;
             break ;
          case "forceUpdate" :
             this.forceUpdate() ;
             break ;
          case "afterCreateGame" :
             this.ref_JouerCreateurComponent.current.state.game   = action.game         ;
             this.ref_JouerCreateurComponent.current.state.word   = action.word         ;
             this.ref_JouerCreateurComponent.current.state.active = true                ;
             break ;
          case "afterJoinGame"   :
            this.ref_JouerComponent.current.state.game  = action.game                   ;
            this.ref_JouerComponent.current.state.active= true                          ;
            break ;
        }
    }

    menuEvent(selectedMenuItem)
    {
      if (selectedMenuItem != this.state.selectedMenuItem) {
        this.state.selectedMenuItem = selectedMenuItem ;
        this.ref_MenuGeneralComponent.current.state.changeSelectedMenuItemFunction(this.ref_MenuGeneralComponent.current,selectedMenuItem) ;
        if (this.selectedComponent==null) this.selectedComponent = this.ref_RejoindrePartieComponent ;
        var newSelectedComponent = null ;
        switch (selectedMenuItem)
        {
          case commons.div_MeConnecter      : newSelectedComponent = this.ref_MeConnecterComponent      ;break ;
          case commons.div_MeDeconnecter    : newSelectedComponent = this.ref_RejoindrePartieComponent  ;break ;
          case commons.div_CreerCompte      : newSelectedComponent = this.ref_CreerCompteComponent      ;break ;
          case commons.div_CreerPartie      : newSelectedComponent = this.ref_CreerPartieComponent      ;break ;
          case commons.div_RejoindrePartie  : newSelectedComponent = this.ref_RejoindrePartieComponent  ;break ;
          case commons.div_Jouer            : newSelectedComponent = this.ref_JouerComponent            ;break ;
          case commons.div_JouerCreateur    : newSelectedComponent = this.ref_JouerCreateurComponent    ;break ;
          case commons.div_HistPartiesCreees: newSelectedComponent = this.ref_HistPartiesCreeesComponent;break ;
          case commons.div_PartiesEnCours   : newSelectedComponent = this.ref_PartiesEnCoursComponent   ;break ;
          case commons.div_HistParties      : newSelectedComponent = this.ref_HistPartiesComponent      ;break ;
          case commons.div_GestionDroits    : newSelectedComponent = this.ref_GestionDroitsComponent    ;break ;
          case commons.div_ReferentielMots  : newSelectedComponent = this.ref_ReferentielMotsComponent  ;break ;
        }
        this.displayError("") ;
        if (selectedMenuItem==commons.div_MeDeconnecter) this.disconnect() ;
        // alert("Sel:"+this.selectedComponent.current.constructor.name+" newsel: "+newSelectedComponent.current.constructor.name) ;
        if (newSelectedComponent!=null) 
        { 
          this.selectedComponent.current.state.changeDisplayFunction(false) ; 
          newSelectedComponent  .current.state.changeDisplayFunction(true ) ; 
          this.selectedComponent = newSelectedComponent ; 
        }
      }
    } 
    
    disconnect (){
      fetch(
          "/disconnect", { method:  'POST',
                        headers: {'Accept': 'application/json','Content-Type': 'application/json'},
                        body:    JSON.stringify({ playerId:commons.gPlayer.playerId})
                      } )
          .then((res ) => { return res.json() })
          .then((json) => { 
              if (json==null) json={errorText:"Pas de répose du serveur"};
              else 
             {
                 commons.gPlayer.playerName = json.playerName ;
                 commons.gPlayer.role       = commons.role_NonConnecte ;
                 this.actionProcess({actionName:"forceUpdate"} ) ;
                 this.actionProcess({actionName:"changeRole" } ) ;
             }
          }
      )
  }

  displayError(error) {this.ref_ErrorComponent.current.state.displayErrorFunction(error) ; }

 } // PictionaryComponent

// Affichage d'un message en utilisant la syntaxe JSX et React
ReactDOM.render(<PictionaryComponent />, document.getElementById('root'));

// signale au serveur que l'utilisateur a "fermé" la page HTML 
window.addEventListener("beforeunload", function () {
  fetch("/exit", { 
    method:  'PUT',
    headers: {'Accept': 'application/json','Content-Type': 'application/json'},
    body:    JSON.stringify({ playerId : commons.gPlayer.playerId })
    } )
});
