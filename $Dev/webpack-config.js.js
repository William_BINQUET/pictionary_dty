﻿module.exports = 
  {
    devtool: 'source-map',

    mode: "development",
    output: { filename: "./app-bundle.js"},
    node: { fs: 'empty' },
    resolve: { extensions: ['.Webpack.js', '.web.js', '.ts', '.js', '.jsx', '.tsx'] },
     module: 
     {
       rules: [  
         {  
           test: /\.tsx$/,    
           exclude: /(node_modules|bower_components)/,  
           use: {   loader: 'ts-loader'     }  
          } 
        ]
      },
  }
