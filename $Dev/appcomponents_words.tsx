﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Composant React pour obtenir et modifier le référentiel de mots pour les administrateurs: 
// - ReferentielMotsComponent
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


import * as commons from './commons';
import * as genprocs  from './appcomponents_genprocs' ;

declare var require: any
var React    = require('react');

export class ReferentielMotsComponent extends React.Component {
    constructor (props) 
    { super (props) ; genprocs.set_Props(this,props) ; 
      this.Validate_Click   = this.Validate_Click.bind(this) ;
      this.Actualize_Click  = this.Actualize_Click .bind(this) ;
      this.LoadWords        = this.LoadWords     .bind(this) ;
      this.Update           = this.Update        .bind(this) ;
      this.state.words      = []                             ;
    }
    
    render() {  
        return ((!this.state.display) || 
            <div className="content">
            <label class="label is-medium">Gestion de la liste des mots a dessiner ou deviner</label>
                <label class="label help">Dans chaque ligne, entrez un mot ou plusieurs mots séparés par des virgules</label>
                <textarea class="textarea" spellCheck="false" rows="20" id="ReferentielMotsComponent_input" defaultValue={this.state.lines}></textarea> 
                <br/>
                <div class="field has-addons" id="errorDiv" >
                   <div class="control"><button class="button is-primary" onClick={this.Actualize_Click}>Actualiser</button></div>
                   <p>&nbsp;&nbsp;</p>
                   <div class="control"><button class="button is-primary" onClick={this.Validate_Click }>Valider   </button></div>
                </div>
            </div>
             ) 
        }

        LoadWords()
        {
          this.state.lines="" ;
          fetch( // /getWords(OUT) words[], error
          "/getWords", { method:  'GET', headers: {'Accept': 'application/json','Content-Type': 'application/json'}})
          .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
          { 
            this.state.words = json.words ; 
            for (var i=0;i<this.state.words.length;i++) this.state.lines+=this.state.words[i]+"\r\n" ;  
            this.forceUpdate() ;
          }})
          .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)}) ;
        }

        Actualize_Click() { 
          this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
          this.LoadWords() ; this.state.display=false ; 
          this.forceUpdate() ; 
          // forceUpdate() ne fonctionnant pas pour la mise à jour du textArea, on fait un premier force Update pour cacher le component, puis on le montre via un Timer
          window.setTimeout(this.Update,100) ;
        } 

        Update() {this.state.display=true ; this.forceUpdate() ;}

        Validate_Click()
        {
          this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
          var lines = (document.getElementById("ReferentielMotsComponent_input") as HTMLInputElement).value.replace(',','\n') ;
          var wordsIn  = lines.split('\n') ;
          var wordsOut = [] ;
          for(var i=0;i<wordsIn.length;i++) { wordsIn[i]=commons.removeAccents(wordsIn[i].trim().toUpperCase()) ; if (wordsIn[i]!="") wordsOut.push(wordsIn[i]) ; }
          fetch( // /setWords(OUT) words[], error
          "/setWords", { method:  'POST', headers: {'Accept': 'application/json','Content-Type': 'application/json'}, 
                         body:    JSON.stringify({ oldWords: this.state.words, newWords: wordsOut})})
          .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
          { 
            this.state.words = wordsOut ;   
            this.state.actionProcessFunction({actionName:"displayError",errorText:'!La nouvelle liste de mots a été enregistrée'}) ;
            this.forceUpdate() ;
          }})
          .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)}) ;
        }

        changeDisplay(vdisplay) 
        {
           if (vdisplay && this.state.words.length==0 && commons.gPlayer.role=="A") this.LoadWords() ;
           genprocs.change_Display(this,vdisplay) ; 
        }
        
} // ReferentielMotsComponent ================================================

