taskkill /im node.exe /F
call node_modules/.bin/node-sass MyBulmaStyles.scss -o dist
call node_modules/.bin/webpack appcomponents_games.tsx appcomponents_players.tsx appcomponents_errors.tsx appcomponents_words.tsx appcomponents_genprocs.tsx  menugeneral.tsx app.tsx --config webpack-config.js
start node --inspect server.js 