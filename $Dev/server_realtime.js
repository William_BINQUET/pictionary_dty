///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Module de gestion des rafraichissements/mise à jour des informations clients. 
// (les données échangées sont l'image du pictionary, les informations sur la partie, et les informations sur les parties pouvant être rejointes).   
// - refreshActions_init(withSocketIo, server)
// - refreshActions_ClearItem(item)
// - refreshActions_GetItem(thePlayerId)  
// - refreshActions_AddItem(thePlayerId) 
// - RefreshAction_UpdateItem(playerId,imageData,game,games)
// - RefreshAction_RemoveItem(playerId)
// - refreshActions_Get(playerId)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

'use strict';

module.exports = {refreshActions_init, RefreshAction_UpdateItem, refreshActions_AddItem, RefreshAction_RemoveItem, refreshActions_Get, refreshActions_ClearItem}

// mode de fonctionnement du serveur
var socketIo            = require('socket.io');
var useSocketIo         = false ; 
var ioServer            = null  ;

// variables spécifiques au serveur Pictionary
var refreshActionsList  = [] ;

//================================ Refresh ====================================== 
// structure de type "refreshActions" :
//    socket             <socket associé au playerId>
//    playerId           <identifiant du joueur destinataire de l'action>
//    imageData          <imageData (null si pas de refresh de l'image)
//    game               <item de games>
//    round              <structure pour les info de fin de manche ou de partie>
//    games              <liste de parties en attente>
// structure de type "round" :
//    roundsCount <nombres de manches de la partie>
//    roundIndex  <Index de la nouvelle manche en cours (partie termininée si roundIndex=roundsCount>
//    solution    <Score dans la partie>
//    finderId    <Identifiant du joueur ayant donné la solution ou -1              si manche terminée à l'initiative du créateur>
//    finderName  <Nom         du joueur ayant donné la solution ou nom du créateur si manche terminée à l'initiative du créateur>

function refreshActions_init(withSocketIo, server) {
  useSocketIo = withSocketIo ;
  ioServer    = socketIo(server);

  ioServer.on('connection', function (socket) {
    socket.on('init', function (data) {
      refreshActions_GetItem(data.playerId).socket = socket;
    });
  });
}

function refreshActions_ClearItem(item) 
{
  item.imageData=null ; item.game=null ; item.round=null ; item.games=null ;    
}

function refreshActions_GetItem(thePlayerId)  
{
  var result=null ;
  for (var i=0;i<refreshActionsList.length && result==null;i++) if (thePlayerId==refreshActionsList[i].playerId) result=refreshActionsList[i] ;
  return result ;
}

function refreshActions_AddItem(thePlayerId) // (OUT) null 
{  
   var item = refreshActions_GetItem(thePlayerId)  ; // normalement item est toujours null (non trouvé dans la liste) 
   if (item==null) item = { playerId:thePlayerId } ; 
   refreshActions_ClearItem(item) ;
   refreshActionsList.push(item) ;
}

function RefreshAction_UpdateItem(playerId,imageData,game,games) // (OUT) null 
{ // paramètres : playerId (si -1 maj de tous les items) et les paramètres ne correspôndant pas a un changement à null 
  var items = refreshActionsList ;
  if (playerId!=-1) { var it = refreshActions_GetItem(playerId) ; items = it==null ? [] : [it] ;}  // normalement item n'est jamais null (toujours trouvé dans la liste)
  for (var i=0;i<items.length;i++)
  {
    var item = items[i] ; 
    var creatorConcerned = game!=null && item.playerId == game.creatorId ;
    var playerConcerned  = playerId = item.playerId ;
    if (game!=null) for (var j=0;j<game.players.length;j++) if (item.playerId==game.players[j].id) playerConcerned=true ;
    var itemChanged      = false  ;
    if (imageData!=null && playerConcerned                     ) { item.imageData = imageData ; itemChanged = true ;}
    if (game     !=null && (playerConcerned|| creatorConcerned)) { item.game      = game      ; itemChanged = true ;}
    if (games    !=null)                                         { item.games     = games     ; itemChanged = true ;}
    if (itemChanged && useSocketIo)
    {
      // transmettre refresh à la connection de playerID
      var saveSocket  = item.socket    ;
      item.socket     = null           ;
      saveSocket.emit('refresh', item) ;
      item.socket     = saveSocket     ;
      refreshActions_ClearItem(item)   ;
    }
  } 
}

function RefreshAction_RemoveItem(playerId) // (OUT) null 
{ 
  for (var i=refreshActionsList.length-1;i>=0 ;i--) if (playerId==refreshActionsList[i].playerId) refreshActionsList.splice(i,1);
}

function refreshActions_Get(playerId) // (OUT) refreshActions or null if no refresh action 
{
  var item = refreshActions_GetItem(playerId)  ; // normalement item n'est jamais null (toujours trouvé dans la liste) 
  return (item!=null && (item.imageData!=null || item.game!=null || item.games!=null))?item:null ;
}
