﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Composant React gérant le menu général (bandeau de choix du composant actif) :
// - MenuGeneralComponent 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

import * as commons from './commons';

declare var require: any
var React = require('react');

export class MenuGeneralComponent extends React.Component {
    constructor (props) // props { role menuOption gameStatus("","C","J") MenuEvent }
    { 
        super (props) ;
        this.state = { menuOption : props.menuOption, changeSelectedMenuItemFunction : this.changeSelectedMenuItem } ;
        this.menu_MeConnecter_Click       = this.menu_MeConnecter_Click      .bind(this);
        this.menu_CreerCompte_Click       = this.menu_CreerCompte_Click      .bind(this);
        this.menu_MeDeconnecter_Click     = this.menu_MeDeconnecter_Click    .bind(this);
        this.menu_CreerPartie_Click       = this.menu_CreerPartie_Click      .bind(this);
        this.menu_RejoindrePartie_Click   = this.menu_RejoindrePartie_Click  .bind(this);
        this.menu_Jouer_Click             = this.menu_Jouer_Click            .bind(this);
        this.menu_JouerCreateur_Click     = this.menu_JouerCreateur_Click    .bind(this);
        this.menu_HistPartiesCreees_Click = this.menu_HistPartiesCreees_Click.bind(this);
        this.menu_PartiesEnCours_Click    = this.menu_PartiesEnCours_Click   .bind(this);
        this.menu_HistParties_Click       = this.menu_HistParties_Click      .bind(this);
        this.menu_GestionDroits_Click     = this.menu_GestionDroits_Click    .bind(this);
        this.menu_ReferentielMots_Click   = this.menu_ReferentielMots_Click  .bind(this);
        this.changeSelectedMenuItem       = this.changeSelectedMenuItem      .bind(this);
    } 
    
    changeSelectedMenuItem(theComponent,vMenuOption)  { theComponent.state.menuOption=vMenuOption ; theComponent.forceUpdate() ; }

    render() {
        return (
            <aside class="menu">
                <p class="menu-label">{commons.menu_TitreConnexion}</p>
                <ul class="menu-list">
                {commons.gPlayer.role==commons.role_NonConnecte?(<li><a onClick={this.menu_MeConnecter_Click     } class={(this.state.menuOption==commons.div_MeConnecter      ?"is-active":"")}>{commons.menu_MeConnecter       }</a></li>):null}
                {commons.doNothing()                           ?(<li><a onClick={this.menu_CreerCompte_Click     } class={(this.state.menuOption==commons.div_CreerCompte      ?"is-active":"")}>{commons.menu_CreerCompte       }</a></li>):null}
                {commons.gPlayer.role!=commons.role_NonConnecte?(<li><a onClick={this.menu_MeDeconnecter_Click   } class={(this.state.menuOption==commons.div_MeDeconnecter    ?"is-active":"")}>{commons.menu_MeDeconnecter     }</a></li>):null}
                </ul>

                <p class="menu-label">{commons.menu_TitreJeu}</p>
                <ul class="menu-list">
                {commons.gPlayer.gameStatus==commons.game_Attente       ?(<li><a onClick={this.menu_CreerPartie_Click      } class={(this.state.menuOption==commons.div_CreerPartie       ?"is-active":"")}>{commons.menu_CreerPartie      }</a></li>):null}
                {commons.gPlayer.gameStatus==commons.game_Attente       ?(<li><a onClick={this.menu_RejoindrePartie_Click  } class={(this.state.menuOption==commons.div_RejoindrePartie   ?"is-active":"")}>{commons.menu_RejoindrePartie  }</a></li>):null}
                {commons.gPlayer.gameStatus==commons.game_Jouant        ?(<li><a onClick={this.menu_Jouer_Click            } class={(this.state.menuOption==commons.div_Jouer             ?"is-active":"")}>{commons.menu_Jouer            }</a></li>):null}
                {commons.gPlayer.gameStatus==commons.game_JouantCreateur?(<li><a onClick={this.menu_JouerCreateur_Click    } class={(this.state.menuOption==commons.div_JouerCreateur     ?"is-active":"")}>{commons.menu_JouerCreateur    }</a></li>):null}
                {commons.gPlayer.role!=commons.role_NonConnecte         ?(<li><a onClick={this.menu_HistPartiesCreees_Click} class={(this.state.menuOption==commons.div_HistPartiesCreees ?"is-active":"")}>{commons.menu_HistPartiesCreees}</a></li>):null}
                </ul>

                { commons.gPlayer.role!=commons.role_Admin || 
                    <p class="menu-label">{commons.menu_TitreAdministration} </p> 
                }
                { commons.gPlayer.role!=commons.role_Admin || 
                    <ul class="menu-list">
                    <li><a onClick={this.menu_PartiesEnCours_Click } class={(this.state.menuOption==commons.div_PartiesEnCours       ?"is-active":"")}>{commons.menu_PartiesEnCours      }</a></li>
                    <li><a onClick={this.menu_HistParties_Click    } class={(this.state.menuOption==commons.div_HistParties          ?"is-active":"")}>{commons.menu_HistParties         }</a></li>
                    <li><a onClick={this.menu_GestionDroits_Click  } class={(this.state.menuOption==commons.div_GestionDroits        ?"is-active":"")}>{commons.menu_GestionDroits       }</a></li>
                    <li><a onClick={this.menu_ReferentielMots_Click} class={(this.state.menuOption==commons.div_ReferentielMots      ?"is-active":"")}>{commons.menu_ReferentielMots     }</a></li>
                    </ul>
                }
            </aside>
        );
     }
  
    menu_MeConnecter_Click       () { this.props.menuEvent(commons.div_MeConnecter      ) ; this.forceUpdate();}
    menu_MeDeconnecter_Click     () { this.props.menuEvent(commons.div_MeDeconnecter    ) ; this.forceUpdate();}
    menu_CreerCompte_Click       () { this.props.menuEvent(commons.div_CreerCompte      ) ; this.forceUpdate();}
    menu_CreerPartie_Click       () { this.props.menuEvent(commons.div_CreerPartie      ) ; this.forceUpdate();}
    menu_RejoindrePartie_Click   () { this.props.menuEvent(commons.div_RejoindrePartie  ) ; this.forceUpdate();}
    menu_Jouer_Click             () { this.props.menuEvent(commons.div_Jouer            ) ; this.forceUpdate();}
    menu_JouerCreateur_Click     () { this.props.menuEvent(commons.div_JouerCreateur    ) ; this.forceUpdate();}
    menu_HistPartiesCreees_Click () { this.props.menuEvent(commons.div_HistPartiesCreees) ; this.forceUpdate();}
    menu_PartiesEnCours_Click    () { this.props.menuEvent(commons.div_PartiesEnCours   ) ; this.forceUpdate();}
    menu_HistParties_Click       () { this.props.menuEvent(commons.div_HistParties      ) ; this.forceUpdate();}
    menu_GestionDroits_Click     () { this.props.menuEvent(commons.div_GestionDroits    ) ; this.forceUpdate();}
    menu_ReferentielMots_Click   () { this.props.menuEvent(commons.div_ReferentielMots  ) ; this.forceUpdate();}

} // MenuGeneralComponent

