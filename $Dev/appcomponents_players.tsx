﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Composants React de création, connexion à un compte et modification des droits pour l'administrateur : 
// - MeConnecterComponent  
// - MeDeconnecterComponent  
// - CreerCompteComponent  
// - GestionDroitsComponent 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

import * as commons from './commons';
import * as genprocs  from './appcomponents_genprocs' ;

declare var require: any
var React    = require('react');

// =============================================================================================================
export class MeConnecterComponent extends React.Component {
    constructor(props){ super(props); genprocs.set_Props(this,props) ; this.meConnecter_Click = this.meConnecter_Click.bind(this);}
    
    render() {  return ( (!this.state.display) ||
        <div className="content">
          <label class="label is-medium">Connexion</label>
          <label class="label">Nom du joueur</label>
          <input class="input" id="MeConnecterComponent_login"    type="text"     placeholder="Nom du joueur"/>
          <br/><br/>
          <label class="label">Mot de passe</label>
          <input class="input" id="MeConnecterComponent_password" type="password" placeholder="Mot de passe"/>
          <br/><br/>
          <button class="button is-primary"  onClick={this.meConnecter_Click }>Me connecter</button>
       </div>
        ) }
    
    changeDisplay(vdisplay) { genprocs.change_Display(this,vdisplay) ; }
    
    meConnecter_Click (){
        this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
        var theLogin    = (document.getElementById("MeConnecterComponent_login"   )as HTMLInputElement).value.trim() ;
        var thePassword = (document.getElementById("MeConnecterComponent_password")as HTMLInputElement).value.trim() ;
        if (thePassword==""    || thePassword==null     ) this.state.actionProcessFunction({actionName:"displayError",errorText:"Mot de passe manquant"}) ;
        else if (theLogin =="") this.state.actionProcessFunction({actionName:"displayError",errorText:"Nom d'utilisateur vide"}) ;
        else fetch( // /connect (IN) playerId,playerName, password (OUT) role, error
                    "/connect", { method:  'POST', headers: {'Accept': 'application/json','Content-Type': 'application/json'},
                     body:    JSON.stringify({ playerName : theLogin, playerId:commons.gPlayer.playerId, password : thePassword}) } )
              .then((res ) => {if (res.ok) return res.json() ; else throw new Error("")}).then((json) => { if (genprocs.fetchOk(this,json)) 
               {
                commons.gPlayer.playerName = theLogin ;
                commons.gPlayer.role       = json.role ;
                this.state.actionProcessFunction({actionName:"forceUpdate"} ) ; // to display anonymous playerName
                this.state.actionProcessFunction({actionName:"changeRole" } ) ;
             } } )  
             .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})


    }
} // MeConnecterComponent =====================================================================

export class MeDeconnecterComponent extends React.Component {
    constructor (props) { super (props) ; genprocs.set_Props(this,props) ; }
    
    render() {  return ((!this.state.display) ||
      <label class="label is-medium">Déconnexion</label>
      ) }
    
    changeDisplay(vdisplay) { genprocs.change_Display(this,vdisplay) ; }

 } // MeDeconnecterComponent ================================================================

export class CreerCompteComponent extends React.Component {
    constructor (props) { super (props) ; genprocs.set_Props(this,props) ; this.creerCompte_Click = this.creerCompte_Click.bind(this) ; }
 
    render() {  return ( (!this.state.display) ||
        <div className="content">
          <label class="label is-medium">Création d'un compte utilisateur</label>
          <label class="label">Nom du joueur</label>
          <input class="input" id="CreerCompteComponent_login"    type="text"     placeholder="Nom du joueur"/>
          <br/><br/>
          <label class="label">Mot de passe</label>
          <input class="input" id="CreerCompteComponent_password" type="password" placeholder="Mot de passe"/>
          <br/><br/>
          <label class="label">Confirmation du mot de passe</label>
          <input class="input" id="CreerCompteComponent_confirmedPassword" type="password" placeholder="Mot de passe"/>
          <br/><br/>
          <button class="button is-primary"  onClick={this.creerCompte_Click }>Créer le compte</button>
       </div>
        ) }
    
        creerCompte_Click (){
            this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
            var theLogin             = (document.getElementById("CreerCompteComponent_login"            )as HTMLInputElement).value ;
            var thePassword          = (document.getElementById("CreerCompteComponent_password"         )as HTMLInputElement).value ;
            var theConfirmedPassword = (document.getElementById("CreerCompteComponent_confirmedPassword")as HTMLInputElement).value ;
            if (theLogin =="" || theLogin.indexOf("#")>=0) this.state.actionProcessFunction({actionName:"displayError",errorText:"Nom d'utilisateur vide ou contenant le caractère '#'"})
            else if (thePassword==""                     ) this.state.actionProcessFunction({actionName:"displayError",errorText:"Mot de passe manquant"}) ;
            else if (thePassword!=theConfirmedPassword   ) this.state.actionProcessFunction({actionName:"displayError",errorText:"Il y a une difference dans les mots de passe"}) ;
            else fetch(
                "/register", { method:  'POST',
                              headers: {'Accept': 'application/json','Content-Type': 'application/json'},
                              body:    JSON.stringify({ playerName : theLogin, password : thePassword})
                            } )
                .then((res ) => { return res.ok?res.json():null })
                .then((json) => { 
                    if (json==null) json={error:"Pas de répose du serveur"};
                    if (json.error!="") this.state.actionProcessFunction({actionName:"displayError",errorText:json.error} ) ;
                    else this.state.actionProcessFunction({actionName:"displayError",errorText:'!Le nouvel utilisateur "'+theLogin+'" a été enregistré'}) ;
                }
            )
        }
        
         changeDisplay(vdisplay) { genprocs.change_Display(this,vdisplay) ; }
} // CreerCompteComponent ===================================================================

export class GestionDroitsComponent extends React.Component {
    constructor (props) { 
        super (props) ; genprocs.set_Props(this,props) ; 
        this.GestionDroitsComponent_Click = this.GestionDroitsComponent_Click.bind(this)
    }
    
    render() {  return ( (!this.state.display) ||
        <div className="content">
          <label class="label is-medium">Gestion des droits utilisateur</label>
            <label class="label">Nom du joueur</label>
            <input class="input" id="GestionDroitsComponent_login"  type="text"     placeholder="Nom d'utilisateur"/>
            <br/><br/>
            <div class="field has-addons" id="errorDiv" >
                <div class="control"><button class="button is-primary is-small"  onClick={()=>this.GestionDroitsComponent_Click("" ) }>Supprimer le compte</button></div>
                <p>&nbsp;&nbsp;</p>
                <div class="control"><button class="button is-primary is-small"  onClick={()=>this.GestionDroitsComponent_Click("C") }>Supprimer les droits administrateur</button></div>
                <p>&nbsp;&nbsp;</p>
                <div class="control"><button class="button is-primary is-small"  onClick={()=>this.GestionDroitsComponent_Click("A") }>Donner des droits administrateur</button></div>
            </div>
       </div>
        ) }
    
        GestionDroitsComponent_Click(theRole)
        {
            this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
            var thePlayerName = (document.getElementById("GestionDroitsComponent_login")as HTMLInputElement).value ;
            if (thePlayerName =="" || thePlayerName.indexOf("#")>=0) this.state.actionProcessFunction({actionName:"displayError",errorText:"Nom d'utilisateur vide ou contenant le caractère '#'"})
            else fetch( // /rights (IN) playerName,role (OUT) error     //  role="A"(Admin) / "C"(utilisateur lambda connecté) /"" (supprimer le compte)  
                "/rights", { method:  'POST',
                              headers: {'Accept': 'application/json','Content-Type': 'application/json'},
                              body:    JSON.stringify({ playerName:thePlayerName,role:theRole})
                            } )
                .then((res ) => { return res.ok?res.json():null })
                .then((json) => { 
                   if (json==null) json={error:"Pas de répose du serveur"};
                   if (json.error!="") this.state.actionProcessFunction({actionName:"displayError",errorText:json.error} ) ;
                   else this.state.actionProcessFunction({actionName:"displayError",errorText:
                      (theRole==""?"!L'utilisateur '"+thePlayerName+"' a été supprimé":
                                   "!les droits administrateurs de l'utilisateur '"+thePlayerName+"'ont été "+(theRole=="A"?" accordés":" supprimés"))} );
                }
            )
            .catch(function (error) { this.state.actionProcessFunction({actionName:"displayError",errorText:error} ) ;})
        }
        
        changeDisplay(vdisplay) { genprocs.change_Display(this,vdisplay) ; }
} // GestionDroitsComponent

