﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Composants React pour rejoindre et créer des parties, jouer et afficher la liste des parties :
// - CreerPartieComponent  
// - RejoindrePartieComponent  
// - JouerComponent  
// - JouerCreateurComponent et JouerCreateurCanvasComponent
// - HistPartiesCreeesComponent  
// - HistPartiesComponent 
// - PartiesEnCoursComponent 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* eslint-disable no-mixed-spaces-and-tabs */

import * as commons from './commons';
import * as genprocs  from './appcomponents_genprocs' ;

declare var require: any
var React    = require('react');

export class CreerPartieComponent extends React.Component {
    constructor (props) 
    { 
       super (props) ;  genprocs.set_Props(this,props) ; this.creerPartie_Click = this.creerPartie_Click.bind(this) 
    }
    
    render() {  return ((!this.state.display) ||
        <div className="content">
          <label class="label is-medium">Création d'une nouvelle partie</label>
          <label class="label">Nombre de manches</label>
          <input class="input" id="CreerPartieComponent_roundsCount" defaultValue="3" />
          <br/><br/>
           <button class="button is-primary" onClick={this.creerPartie_Click }>Créer la partie</button>
        </div>
    ) }

   changeDisplay(vdisplay) {  genprocs.change_Display(this,vdisplay) ; }
   
    creerPartie_Click (){
       this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
       var roundsCountString = (document.getElementById("CreerPartieComponent_roundsCount")as HTMLInputElement).value ;
       var theRoundsCount    = parseInt(roundsCountString) ;
       if (isNaN(theRoundsCount) || theRoundsCount<0 || theRoundsCount>10)  this.state.actionProcessFunction({actionName:"displayError",errorText:"Nombre de manches incorrect (9 manches maximum)"}) ;
       else fetch( // createGame (IN) playerId playerName RoundsCount (OUT) game, word, error
            "/createGame", { method:'POST', headers:{'Accept': 'application/json','Content-Type': 'application/json'}, 
                            body:JSON.stringify({ playerId:commons.gPlayer.playerId, playerName:commons.gPlayer.playerName,roundsCount:theRoundsCount}) } )
            .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
               {
                  this.state.actionProcessFunction({actionName:"afterCreateGame",word : json.word, game:json.game} ) ;
                  commons.gPlayer.gameStatus = commons.game_JouantCreateur ;
                  this.state.actionProcessFunction({actionName:"changeGame"} ) ;
                } } ) 
            .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})
    }
            
} // CreerPartieComponent ================================================================

export class RejoindrePartieComponent extends React.Component {

    constructor (props) { 
        super (props) ; genprocs.set_Props(this,props) ; 
        this.refresh                   = this.refresh        .bind(this) ;
        this.joinGame_Click            = this.joinGame_Click .bind(this) ; 
        this.renderGames               = this.renderGames    .bind(this) ; 
        this.state.refreshFunction     = this.refresh ;
        this.state.games               = [] ;
        fetch( // /getGames (OUT) games
        "/getGames", { method:'GET', headers:{'Accept': 'application/json','Content-Type': 'application/json'}, } )
        .then((res ) => { return res.ok?res.json():null })
        .then((json) => { 
           if (json==null) json={error:"Pas de répose du serveur"};
           else this.state.games = json.games ; 
        }
    )

    }
    
    render() {  return ((!this.state.display) ||
        <div>
          <label class="label is-medium">Choix d'une partie</label>
          <table>{this.renderGames()}</table>
          <br></br>
          <button class="button is-primary" onClick={this.joinGame_Click }>Rejoindre la partie selectionnée</button>
        </div> ) 
    }
    
    renderGames()
    {
       var games = this.state.games ; 
       var result = [] ;
       var j=0 ;
       for (var i = 0; i < games.length; i++) if (!games[i].started) {
         var theId       = "RejoindrePartieComponent_Radio"+j++ ;
         result.push(<div class="field has-addons"><br></br><label class="radio"> <input type="radio" id={theId} name="RejoindrePartieComponent_Radiogroup"/></label><p>&nbsp;&nbsp;</p>
                          <textarea class="textarea is-info is-small" readOnly spellCheck="false" rows="2" cols="100" id={theId} value={genprocs.gameToString(games[i],"JOIN")} ></textarea>
                      </div>) ;
        }
       return <div class="field">{result}</div> ;
    }

    joinGame_Click()
    {
      this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
      var selectedIndex = -1 ;   
      var j=0 ;
      for (var i=0;i<this.state.games.length;i++) if (!this.state.games[i].started) {
        var theRadio= document.getElementById("RejoindrePartieComponent_Radio"+j++) ;
        if ((theRadio as HTMLInputElement).checked) selectedIndex = i ;
      }
      if (selectedIndex<0) this.state.actionProcessFunction({actionName:"displayError",errorText:"Aucune partie n'est sélectionnée"}) ;
      else {
        fetch( // joinGame (IN) gameId,playerId,playerName (OUT) game, error
        "/joinGame", { method:'POST', headers:{'Accept': 'application/json','Content-Type': 'application/json'}, 
                        body:JSON.stringify({ gameId : this.state.games[selectedIndex].gameId, playerId : commons.gPlayer.playerId, 
                                              playerName:commons.gPlayer.playerName}) } )
        .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
           {
            this.state.actionProcessFunction({actionName:"afterJoinGame", game:json.game} ) ;
            commons.gPlayer.gameStatus = commons.game_Jouant ; 
            this.state.actionProcessFunction({actionName:"changeGame"} ) ;
           } } ) 
        .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})
      }
    }

    refresh(refreshActions) 
    {
       if (refreshActions.games!=null) { this.state.games=refreshActions.games ; this.forceUpdate() ; }
    }

    changeDisplay(vdisplay) { genprocs.change_Display(this,vdisplay) ; }
} // RejoindrePartieComponent ============================================================

export class JouerComponent extends React.Component {
    constructor (props) 
    { 
        super (props) ; genprocs.set_Props(this,props) ; 
        this.refresh                   = this.refresh             .bind(this) ;
        this.propose_Click             = this.propose_Click       .bind(this) ;
        this.leaveGame_Click           = this.leaveGame_Click     .bind(this) ; 
        this.afterRender               = this.afterRender         .bind(this) ;
        this.state.refreshFunction     = this.refresh                         ;
        this.state.imageData           = []                                   ;
        this.state.game                = { dialogs : "" , started : false }   ;
        this.state.active              = false                                ;
        this.state.previousWord        = ""                                   ;
    }

    render() { 
      var dialogsCount        = (this.state.game.dialogs.match(/\n/g) || [0]).length;
      var roundInfo           = "" ;
      if (this.state.game != null) 
            roundInfo = this.state.game.terminated? ( this.state.game.roundIndex>=this.state.game.roundsCount ? "partie terminée":"partie annulée par son créateur") :
                        this.state.game.started   ? "Manche " + (this.state.game.roundIndex+1) + "/" + this.state.game.roundsCount : "Partie en attente" ;
        var result = ((!this.state.display) ||
        <div className="content">
          <label class="label is-medium">Deviner le mot correspondant au dessin</label>
          <textarea class="textarea is-info" rows="3" readOnly spellCheck="false" value={genprocs.gameToString(this.state.game,"ACTIVE")}></textarea> 
          <br></br>
          {  this.state.game.terminated                            ? <label class="label has-background-warning" >{"\u25B6 "+roundInfo}</label> : null }
          { !this.state.game.terminated && !this.state.game.started? <label class="label has-background-info"    >{"\u25B6 "+roundInfo}</label> : null }
          { !this.state.game.terminated &&  this.state.game.started? <label class="label"                        >{roundInfo}          </label> : null }
          <div id="drawingcontainer">
              <canvas width={commons.picture_Width} height={commons.picture_Heigth} id="JouerComponent_Canvas" style={commons.picture_Style}>Votre explorateur Web n'accepte pas les canvas</canvas>
          </div>

          { !this.state.game.terminated &&  this.state.game.started?<label class="label">Proposition de mot</label>              :null }
          { !this.state.game.terminated &&  this.state.game.started?<input class="input" id="JouerComponent_Word"  type="text"/> :null }
          { !this.state.game.terminated &&  this.state.game.started?<br></br>                                                    :null }
          <br></br>
          <div class="field has-addons" id="JouerCreateurComponent_ButtonsDiv" >
            { this.state.game.started && !this.state.game.terminated ? (<div class="control"><button class="button is-primary" onClick={this.propose_Click}>Soumettre ma proposition</button></div>):null }
            <p>&nbsp;&nbsp;</p>            
            <div class="control"><button class="button is-primary" onClick={this.leaveGame_Click  }>Quitter la partie       </button></div>
          </div>
          <br></br>
          <label class="label is-small">Résultat de la manche précédente et événements de la manche en cours</label>
          <textarea class="textarea is-info" readOnly spellCheck="false" rows = {dialogsCount+1} value={this.state.game.dialogs}></textarea> 
        </div>
       ) 
       return result ;
      }  
    
    refresh(refreshActions) 
    {
      if (this.state.active) {
        this.state.previousWord = (refreshActions.game!=null && refreshActions.game.roundIndex>this.state.game.roundIndex)? refreshActions.game.words[refreshActions.game.roundIndex-1]:"" ;
        if (refreshActions.imageData !=null) this.state.imageData = refreshActions.imageData ;   
        if (refreshActions.game      !=null) this.state.game      = refreshActions.game ; 
        if (refreshActions.games     !=null) this.state.games     = refreshActions.games ; 
        if (refreshActions.imageData !=null || refreshActions.game!=null || refreshActions.games!=null) this.forceUpdate() ;
      }
    }

    propose_Click()
    {
      this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
      var theWord = commons.removeAccents((document.getElementById("JouerComponent_Word") as HTMLInputElement).value.trim().toUpperCase()) ;
      if      (!this.state.game.started   ) this.state.actionProcessFunction({actionName:"displayError",errorText:"La partie n'a pas encore démarré"}) ;
      else if (this.state.game.terminated ) this.state.actionProcessFunction({actionName:"displayError",errorText:"La partie est terminée"          }) ;
      else if (theWord==""                ) this.state.actionProcessFunction({actionName:"displayError",errorText:"le mot n'est pas spécifié"       }) ;
      else fetch( // /propose (IN) gameId,playerId,word (OUT) game,success,error  
      "/propose", { method:'POST', headers:{'Accept': 'application/json','Content-Type': 'application/json'}, 
                      body:JSON.stringify({ gameId : this.state.game.gameId, playerId:commons.gPlayer.playerId,word:theWord})})
      .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
         {
           this.state.game = json.game ;
           if (json.success) this.state.previousWord = theWord ;
           this.forceUpdate() ;
           this.state.actionProcessFunction({actionName:"changeWord"} ) ;
         } } )
      .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)}) ;
            this.forceUpdate() ;
    }
    
    leaveGame_Click()
    {  
      this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
      this.state.imageData = []    ;
      this.state.active    = false ;
      commons.gPlayer.gameStatus = commons.game_Attente ; 
      if (!this.state.game.terminated)
      fetch( // / leaveGame (IN) gameId,playerId (OUT) error  
      "/leaveGame", { method:'POST', headers:{'Accept': 'application/json','Content-Type': 'application/json'}, 
                       body:JSON.stringify({ gameId : this.state.game.gameId, playerId:commons.gPlayer.playerId})})
      .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
         { commons.doNothing() } } )
      .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})
      this.state.actionProcessFunction({actionName:"changeGame"} ) ;
    }    

    afterRender() {
        if (this.state.active && this.state.display) 
        {
            this.imageEditorInit() ; 
            this.state.imageEditorContext.clearRect(0,0,commons.picture_Width,commons.picture_Heigth) ;
            var image = new Image  ;
            image.src = this.state.imageData;
            var previousWord = this.state.previousWord ;
            this.imageEditorInit()          ;
            var imageEditorContext = this.state.imageEditorContext ; 
            image.addEventListener("load", function() { 
              imageEditorContext.drawImage(image, 0, 0) ; 
              if (previousWord != "") {imageEditorContext.font = "25px Arial"; imageEditorContext.fillStyle = "green"; imageEditorContext.fillText(previousWord,10,50)}
            });
            this.state.previousWord = "";
        }
      }

    componentDidMount() {this.afterRender() }
      
    componentDidUpdate() {this.afterRender() }
      
    imageEditorInit() {
        // Get the drawing canvas
        this.state.imageEditorCanvas  = document.getElementById('JouerComponent_Canvas') as HTMLCanvasElement;
        this.state.imageEditorContext = this.state.imageEditorCanvas.getContext('2d');
        this.state.imageEditorContext.strokeStyle = '#000';
        this.state.imageEditorContext.lineWidth = 1 ;
    }
    
    changeDisplay(vdisplay) { genprocs.change_Display(this,vdisplay) ;}
    
    
} // JouerComponent ============================================================

export class JouerCreateurCanvasComponent extends React.Component {
  constructor (props) { 
    super (props) ; this.state = {parent : props.parent, started : props.parent.state.started, imageData : props.imageData} ;
    this.imageEditorInit              = this.imageEditorInit             .bind(this);
    this.imageEditorCanvas_mousedown  = this.imageEditorCanvas_mousedown .bind(this);
    this.imageEditorCanvas_mouseup    = this.imageEditorCanvas_mouseup   .bind(this);
    this.imageEditorCanvas_mousemove  = this.imageEditorCanvas_mousemove .bind(this);
    this.imageEditorGetOffset         = this.imageEditorGetOffset        .bind(this);
    this.imageEditorSetDrawAttributes = this.imageEditorSetDrawAttributes.bind(this);
    this.imageEditorGetImage          = this.imageEditorGetImage         .bind(this);
    this.afterRender                  = this.afterRender                 .bind(this);
    this.imageEditorGetImageUrl       = this.imageEditorGetImageUrl      .bind(this);
    this.clearImageData               = this.clearImageData              .bind(this);
  }


    render() { 
      var result =  
        <div id="drawingcontainer">
        <p>Utiliser le click gauche pour tracer et le click droit pour effacer</p>
        <canvas width={commons.picture_Width} height={commons.picture_Heigth} id="JouerCreateurComponent_Canvas" 
            style={this.state.started?commons.picture_Style:commons.picture_DisabledStyle}>Votre explorateur Web n'accepte pas les canvas</canvas>
        </div>
    return result  
  }
  
  afterRender() {  
    this.imageEditorInit() ; 
    if (this.state.imageData.length==0) this.state.imageEditorContext.clearRect(0,0,commons.picture_Width,commons.picture_Heigth) ;
    else {this.state.imageEditorContext.putImageData(this.state.imageData,0,0); this.imageEditorInit() ; }
}
  componentDidUpdate() {this.afterRender()}

  componentDidMount() {this.afterRender()}

  imageEditorInit() {
    // Get the drawing canvas
    this.state.imageEditorCanvas  = document.getElementById('JouerCreateurComponent_Canvas') as HTMLCanvasElement;
    this.state.imageEditorContext = this.state.imageEditorCanvas.getContext('2d');
    this.state.imageEditorContext.strokeStyle = '#000';
    this.state.imageEditorContext.lineWidth = 1 ;
    // Add some event listeners so we can figure out what's happening and run a few functions when they are executed.
    this.state.imageEditorCanvas.addEventListener('mousemove', this.imageEditorCanvas_mousemove, false);
    this.state.imageEditorCanvas.addEventListener('mousedown', this.imageEditorCanvas_mousedown, false);
    this.state.imageEditorCanvas.addEventListener('mouseup'  , this.imageEditorCanvas_mouseup  , false);
  }

    imageEditorSetDrawAttributes(erase)
    {
      this.state.imageEditorCanvas .style.cursor  = erase? "cell" : "default";
      this.state.imageEditorContext.strokeStyle   = erase? "#FFF": "#000"   ;
      this.state.imageEditorContext.lineWidth     = erase? '10'   : '1'      ;
    }

    // ----------------------- ImageEditor Mouse events and functions ---------------------------------------------

    imageEditorCanvas_mousedown(e) {
    	// When the mouse is clicked. Change started to true and move
    	// the initial position to the position of the mouse
    	this.state.imageEditorContext.beginPath();
    	this.state.imageEditorContext.moveTo(this.state.x, this.state.y);
    	var rightClick = false ;
        e = e || window.event;
        if      ("which"  in e) rightClick = e.which  == 3; // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
        else if ("button" in e) rightClick = e.button == 2; // IE, Opera 
    	this.imageEditorSetDrawAttributes(rightClick) ;
    	this.state.drawStarted = true;
    }

    imageEditorCanvas_mousemove(e) {
    	// Get mouse position
    	if(e.offsetX || e.offsetY) {
    		this.state.x = e.pageX - this.imageEditorGetOffset(this.state.imageEditorCanvas).left - window.pageXOffset;
    		this.state.y = e.pageY - this.imageEditorGetOffset(this.state.imageEditorCanvas).top - window.pageYOffset;
    	}
    	else if(e.layerX || e.layerY) {
    		this.state.x = (e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft)- this.imageEditorGetOffset(this.state.imageEditorCanvas).left - window.pageXOffset;
        this.state.y = (e.clientY + document.body.scrollTop  + document.documentElement.scrollTop )- this.imageEditorGetOffset(this.state.imageEditorCanvas).top;
    	}	
    	// If started is true, then draw a line
    	if(this.state.drawStarted) {  this.state.imageEditorContext.lineTo(this.state.x, this.state.y); this.state.imageEditorContext.stroke(); }
    }

    imageEditorCanvas_mouseup() {
        if(this.state.drawStarted) { 
          this.state.drawStarted = false;  
          this.imageEditorSetDrawAttributes(false) ; 
          this.state.parent.state.imageData = this.imageEditorGetImage();
          var theImageData = this.imageEditorGetImageUrl() ;
          fetch( // (IN) gameId,imageData (OUT) error
          "/newImage", { method:  'POST',
                        headers: {'Accept': 'application/json','Content-Type': 'application/json'},
                        body:    JSON.stringify({ gameId : this.state.parent.state.game.gameId, imageData: theImageData })
                      } )
          .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) 
          .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})
        }
    } // Change started to false when the user unclicks the mouse

    // For getting the new ouse position, basically. This gets the position of the canvas element, so we can use it to calculate the mouse position on the canvas
    imageEditorGetOffset(e) {
        var cx = 0;
        var cy = 0;
        while(e && !isNaN(e.offsetLeft) && !isNaN(e.offsetTop)) {
            cx += e.offsetLeft - e.scrollLeft;
            cy += e.offsetTop  - e.scrollTop;
            e   = e.offsetParent;
        }
        return { top: cy, left: cx };
    }

    imageEditorGetImageUrl()
    {
      return this.state.imageEditorCanvas.toDataURL();
    } 

    imageEditorGetImage()
    {
      return this.state.imageEditorContext==null?[]:this.state.imageEditorContext.getImageData(0,0,this.state.imageEditorCanvas.width,this.state.imageEditorCanvas.height);
    } 

    clearImageData() {this.state.imageData = [], this.state.parent.state.imageData = []}
 
} // JouerCreateurCanvasComponent


export class JouerCreateurComponent extends React.Component {
    constructor (props) { 
        super (props) ; genprocs.set_Props(this,props) ; 
        this.refresh                      = this.refresh                     .bind(this);
        this.getNewWord                   = this.getNewWord                  .bind(this);
        this.stopGame_Click               = this.stopGame_Click              .bind(this);
        this.startGame_Click              = this.startGame_Click             .bind(this);
        this.stopRound_Click              = this.stopRound_Click             .bind(this);
        this.state.ref_CanvasComponent    = React.createRef() ;
        this.state.refreshFunction        = this.refresh ;
        this.state.imageData              = []           ;
        this.state.started                = false ;
        this.state.game                   = {dialogs : ""} ;
        this.state.word                   = "" ;
        this.state.active                 = false                                ;
      }

    render() {  
      var dialogsCount = (this.state.game.dialogs.match(/\n/g) || [0]).length;
      var roundInfo    = this.state.game.terminated?" Partie terminée":"Manche "+(this.state.game.roundIndex+1)+"/"+this.state.game.roundsCount+ "  Mot : "+ this.state.word ;
      var result = ((!this.state.display) ||
      <div className="content">
        <label class="label is-medium">Animation de la partie</label>
        <textarea class="textarea is-info" rows="3" readOnly spellCheck="false" value={genprocs.gameToString(this.state.game,"CREATOR")}></textarea> 
        <br></br>
        {  !this.state.game.terminated ?<label class="label">                         {roundInfo}          </label> : null }
        {   this.state.game.terminated ? <label class="label has-background-warning" >{"\u25B6 "+roundInfo}</label> : null } 
        <JouerCreateurCanvasComponent ref={this.state.ref_CanvasComponent} parent={this} imageData={this.state.imageData}/>
        <br></br>
        <div class="field has-addons" id="JouerCreateurComponent_ButtonsDiv" >
            <div class="control">{!this.state.started && this.state.game.players.length>0?
               (<button class="button is-primary" onClick={this.startGame_Click }>Début de partie     </button>):null}</div>
            <p>&nbsp;&nbsp;</p>
            <div class="control">{!this.state.started?
               (<button class="button is-primary" onClick={this.stopGame_Click  }>Annulation          </button>):null}</div>
            <p>&nbsp;&nbsp;</p>
            <div class="control">{this.state.started && this.state.game.roundIndex<this.state.game.roundsCount-1?
               (<button class="button is-primary" onClick={this.stopRound_Click }>Fin de manche       </button>):null}</div>
            <p>&nbsp;&nbsp;</p>
            <div class="control">{this.state.started ?
               (<button class="button is-primary" onClick={this.stopGame_Click  }>Fin de partie       </button>):null}</div>
        </div>
        <br></br>
        <label class="label is-small">Résultat de la manche précédente et événements de ma manche en cours</label>
        <textarea class="textarea is-info" readOnly spellCheck="false" rows = {dialogsCount+1} value={this.state.game.dialogs}></textarea> 
      </div>
     ) 
     return result ;
    }  

changeDisplay(vdisplay) { 
        //var restoreDisplay = vdisplay && !this.state.display ; 
        //if (!vdisplay && this.state.started) this.state.imageData = this.imageEditorGetImage();
        genprocs.change_Display(this,vdisplay) ; 
    }

    refresh(refreshActions) 
    {
      if (this.state.active && refreshActions.game.gameId == this.state.game.gameId) {
        if (refreshActions.game !=null) this.state.game = refreshActions.game ; 
        if (this.state.game.newWordRequired) this.getNewWord() ;
        if (refreshActions.game!=null || refreshActions.round!=null) this.forceUpdate() ;
      }
    }

    getNewWord()
    {
      fetch( // getNewWord (IN) gameId (OUT) word,error   
      "/getNewWord", { method:'POST', headers:{'Accept': 'application/json','Content-Type': 'application/json'}, 
                      body:JSON.stringify({ gameId : this.state.game.gameId})})
      .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
         {
           this.state.word   = json.word ;
           this.state.ref_CanvasComponent.current.clearImageData() ;
           this.state.ref_CanvasComponent.current.forceUpdate();
           this.forceUpdate()
         } } )
      .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})
      } 

    startGame_Click (){
      this.state.started   = true ;
      this.state.ref_CanvasComponent.current.state.started = true ; 
      this.state.ref_CanvasComponent.current.clearImageData() ;
      fetch( // startGame (IN) gameId (OUT) word,error   
      "/startGame", { method:'POST', headers:{'Accept': 'application/json','Content-Type': 'application/json'}, 
                      body:JSON.stringify({ gameId : this.state.game.gameId})})
      .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
         {
           this.state.word = json.word ;
         } } )
      .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})
      this.state.ref_CanvasComponent.current.forceUpdate();
      this.forceUpdate() ;
    }
  
    stopRound_Click () {
     // Signaler une fin de manche et passer à une nouvelle manche en récupérant le mot à dessiner suivant
     fetch( // stopRound(gameId) (OUT) word,error
     "/stopRound", { method:'POST', headers:{'Accept': 'application/json','Content-Type': 'application/json'}, 
                     body:JSON.stringify({ gameId : this.state.game.gameId})})
     .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
        {
          this.state.word = json.word ;
        } } )
     .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})
     this.state.ref_CanvasComponent.current.clearImageData() ;
     this.state.ref_CanvasComponent.current.forceUpdate();
     this.forceUpdate() ;
    }
  
    stopGame_Click (){
      this.state.ref_CanvasComponent.current.clearImageData() ;
      commons.gPlayer.gameStatus = commons.game_Attente ; 
      this.state.started           = false ;
      this.state.ref_CanvasComponent.current.state.started = false ; 
      this.state.active            = false ;
      fetch( // stopRound(gameId) (OUT) word,error
      "/stopGame", { method:'POST', headers:{'Accept': 'application/json','Content-Type': 'application/json'}, 
                      body:JSON.stringify({ gameId : this.state.game.gameId})})
      .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
         {
           this.state.actionProcessFunction({actionName:"changeGame"} ) ;
         } } )
      .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})
      }

} // JouerCreateurComponent ============================================================

export class HistPartiesCreeesComponent extends React.Component {
  constructor (props) 
  { 
    super (props) ; genprocs.set_Props(this,props) ; 
    this.getGamesHistory_Click        = this.getGamesHistory_Click      .bind(this) ;
    this.state.games                  = []                                          ;
  }
  
  render() {  return ((!this.state.display) ||
      <div className="content">
       <label class="label is-medium">Historique des parties créées par {commons.gPlayer.playerName}</label>
      <table>{this.renderGames()}</table>
      <div class="field has-addons" id="JouerCreateurComponent_ButtonsDiv" >
          <div class="control"><button class="button is-primary"  onClick={this.getGamesHistory_Click    }>Actualiser</button></div>
        </div>
      </div>
  )}

  renderGames()
  {
    var games = this.state.games ; 
    var result = [] ;
    for (var i = 0; i < games.length; i++) {
      result.push(<textarea class="textarea is-info is-small" readOnly spellCheck="false" rows="3" cols="100" value={genprocs.gameToString(games[i],"CREATOR")} ></textarea>) ;
      result.push(<br/>)
    }
    return <div class="field">{result}</div> ;
  }

  getGamesHistory_Click()
  {
    this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
    var thePlayerName = commons.gPlayer.playerName ;
    fetch( // /getGamesHistory (IN) playerId (OUT) games[],error  <playerName="" pour tous les joueurs> < games : array of object { dbGameId, gameInfo} >
    "/getGamesHistory", { method:'POST', headers:{'Accept': 'application/json','Content-Type': 'application/json'}, 
                    body:JSON.stringify({ playerName : thePlayerName})})
    .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
       {
         this.state.games = json.games;
         this.forceUpdate()
       } } )
    .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})
  }

  changeDisplay(vdisplay) { genprocs.change_Display(this,vdisplay) ; }
} // HistPartiesCreeesComponent =======================================================================

export class HistPartiesComponent extends React.Component {
    constructor (props) 
    { 
      super (props) ; genprocs.set_Props(this,props) ; 
      this.getGamesHistory_Click        = this.getGamesHistory_Click      .bind(this) ;
      this.deleteGameHistory_Click      = this.deleteGameHistory_Click    .bind(this) ;
      this.deleteAllGamesHistory_Click  = this.deleteAllGamesHistory_Click.bind(this) ;
      this.state.games                  = []                                          ;
    }
    
    render() {  return ((!this.state.display) ||
        <div className="content">
         <label class="label is-medium">Historique des parties créées par tous les utilisateurs</label> 
        <table>{this.renderGames()}</table>
        <div class="field has-addons" id="JouerCreateurComponent_ButtonsDiv" >
            <div class="control"><button class="button is-primary"  onClick={this.getGamesHistory_Click    }>Actualiser</button></div>
            <p>&nbsp;&nbsp;</p>
            <div class="control"><button class="button is-primary" onClick={this.deleteGameHistory_Click}>Supprimer</button></div>
            <p>&nbsp;&nbsp;</p>
            <div class="control"><button class="button is-primary" onClick={this.deleteAllGamesHistory_Click}>Supprimer tout</button></div>
          </div>
        </div>
    )}

    renderGames()
    {
      var games = this.state.games ; 
      var result = [] ;
      for (var i = 0; i < games.length; i++) {
        var theId       = "HistPartiesComponent_Radio"+i ;
        result.push(<div class="field has-addons"><br></br><label class="radio"> <input type="radio" id={theId} name="RejoindrePartieComponent_Radiogroup"/></label><p>&nbsp;&nbsp;</p>
                         <textarea class="textarea is-info is-small" readOnly spellCheck="false" rows="3" cols="100" id={theId} value={genprocs.gameToString(games[i],"CREATOR")} ></textarea>
                     </div>) ;
       }
      return <div class="field">{result}</div> ;
    }

    getGamesHistory_Click()
    {
      this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
      var thePlayerName = "" ;
      fetch( // /getGamesHistory (IN) playerId (OUT) games[],error  <playerName="" pour tous les joueurs> < games : array of object { dbGameId, gameInfo} >
      "/getGamesHistory", { method:'POST', headers:{'Accept': 'application/json','Content-Type': 'application/json'}, 
                      body:JSON.stringify({ playerName : thePlayerName})})
      .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
         {
           this.state.games = json.games;
           this.forceUpdate()
         } } )
      .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})
    }

    deleteAllGamesHistory_Click() { this.deleteGameHistory_Click(false) ; }
    
    deleteGameHistory_Click(one)
    {
      this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
      
      var selectedIndex = -1 ;   
      for (var i=0;i<this.state.games.length && one;i++) {
        var theRadio= document.getElementById("HistPartiesComponent_Radio"+i) ;
        if ((theRadio as HTMLInputElement).checked) selectedIndex = i ;
      }
      if (selectedIndex<0 && one) this.state.actionProcessFunction({actionName:"displayError",errorText:"Aucune partie n'est sélectionnée"}) ;
      else {
      var theDbGameId = one ? this.state.games[selectedIndex]._id :-1 ; 
      fetch( // deleteGameHistory (IN) dbGameId (OUT) error  <dbGameId=-1 pour toutes les parties>
      "/deleteGameHistory", { method:'POST', headers:{'Accept': 'application/json','Content-Type': 'application/json'}, 
                      body:JSON.stringify({ dbGameId : theDbGameId})})
      .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
         {
          commons.doNothing()
         } } )
      .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})
      this.getGamesHistory_Click() ;
      }
    }

    changeDisplay(vdisplay) { genprocs.change_Display(this,vdisplay) ; }
} // HistPartiesComponent =======================================================================

export class PartiesEnCoursComponent extends React.Component {
  constructor (props) { super (props) ; genprocs.set_Props(this,props) ; this.getGames_Click = this.getGames_Click.bind(this) ; this.state.games = [] ;}

  render() {  return ((!this.state.display) ||
     <div className="content">
     <label class="label is-medium">Liste des parties en cours</label>
     <table>{this.renderGames()}</table>
     <button class="button is-primary"  onClick={this.getGames_Click}>Actualiser</button>
     </div>
)}

renderGames()
{
   var games = this.state.games ; 
   var result = [] ;
   for (var i = 0; i < games.length; i++) {
     result.push(<div class="field has-addons"><br></br><label class="label-is-medium"> {i+1} </label><p>&nbsp;&nbsp;</p>
                      <textarea class="textarea is-info is-small" readOnly spellCheck="false" rows="2" cols="100" value={genprocs.gameToString(games[i],"JOIN")} ></textarea>
                  </div>) ;
    }
   return <div class="field">{result}</div> ;
}

getGames_Click()
{
  this.state.actionProcessFunction({actionName:"displayError",errorText:""}) ;
   fetch( // /getGames (OUT) games
                    "/getGames", { method:  'GET', headers: {'Accept': 'application/json','Content-Type': 'application/json'}, })
   .then((res ) => { if (res.ok) return res.json() ; else throw new Error("") }) .then((json) => { if (genprocs.fetchOk(this,json)) 
   { 
     this.state.games = json.games ; 
     this.forceUpdate() ;
   }})
   .catch (error=>{commons.doNothing(error) ; genprocs.fetchOk(this,null)})
  }

  public changeDisplay(vdisplay) {  genprocs.change_Display(this,vdisplay) ; }
} // PartiesEnCoursComponent

