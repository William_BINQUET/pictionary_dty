﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Module de gestion des mots à faire deviner pour chaque partie :
// - setWords (oldWords, newWords)
// - getWords()
// - getRandomWords(roundsCount)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

'use strict';

module.exports = {setWords, getWords, getRandomWords}
var mon  = require('./server_mongo') ;

//====================================================================================================================
//================================ Gestion du referentiel de mots ==================================*/

async function setWords (oldWords, newWords) // (OUT) error
{
  var error   = "problème d'accès au référentiel de mots dans MongoDb" ;
  var dbWords = [] ;
  await getWords().then((result) =>{dbWords = result.words; error = result.error});
  if (oldWords.toString() == dbWords.toString() || dbWords == [] || dbWords == null) 
  {
    await mon.mongoDb_deleteOne('wordsCollection',{}            ).then ((      ) =>{                    }) ;
    await mon.mongoDb_insertOne('wordsCollection',newWords, true).then ((result) =>{ error=result.error }) ;
  }
  else error = "la liste de mots initiale vient d'être modifiée par un autre administrateur";
  return error ;
}

async function getWords() // (OUT) words[],error
{
  var res = {} ;
  await mon.mongoDb_findOne('wordsCollection',{},true)
  .then ((result) =>{ res = { words: result.item==null?[]:result.item, error:result.error } })
  .catch(res= {items: [], error:  "problème d'accès au référentiel de mots dans MongoDb"})
  return res;
}

async function getRandomWords(roundsCount) // (OUT) words, error
{ // Effectue un tirage aléatoire de n mots (n=roundsCount)
  var moreWords  = ["LUNETTE", "POMME", "CAROTTE", "POISSON", "CHAT", "LAPIN", "HELICE", "MARTEAU", "CHAISE"] ;
  var dbWords    = [] ;
  var theWords   = [] ;
  var theError   = "" ;
  await getWords().then((result) =>{dbWords = result.words; theError = result.error});
  for (var k=0; dbWords.length<roundsCount; k++) dbWords.push(moreWords[k])
  for (var i=0; i < roundsCount; i++) 
  {
      var j = Math.floor(Math.random()*dbWords.length) ;
      theWords.push(dbWords[j]) ;
      dbWords.splice(j, 1) 
  } 
  return { words: theWords , error: theError}   ;  
}
