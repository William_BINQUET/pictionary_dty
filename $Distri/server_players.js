///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Module d'inscription et de gestion des droits des joueurs :
// - connect(thePlayerId,thePlayerName,thePassword)
// - register(thePlayerName,thePassword)
// - setRights(thePlayerName,role) 
// - createAccount(thePlayerName,thePassword)
// - getAccount(thePlayerName)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

'use strict';

module.exports = {connect, register, setRights}

var mon                 = require('./server_mongo') ;

// variables spécifiques au serveur Pictionary
var adminRescueName     = "admin#secours"  ; // "#" necessaire pour éviter un conflit avec un joueur/administrateur lambda
var adminRescuePassword = "admin#password" ; 

//====================================================================================================================
//=================================== Gestion des comptes et des droits ==============================================
// // structure de type "account" :
//    playerName        Nom dude l'utilisateur
//    password          Mot de passe
//    role              "A"(Admin) / "C"(utilisateur lambda connecté non admin) 

async function connect(thePlayerId,thePlayerName,thePassword)
// (OUT) role,error
{
var theRole        = "N" ; 
var theError       = ""   ;
if (thePlayerName==adminRescueName) { if (thePassword==adminRescuePassword) theRole="A" ; else theError = "mot de passe erroné" ; }
else 
{
  await getAccount(thePlayerName).then((result) =>{
       theError = result.error ; 
       if (theError=="" && result.account==null) theError=" Nom d'utilisateur inconnu" ;
       else if (result!=null)
       {
         if (result.account.password!=thePassword) theError=" Mot de passe incorrect" ;
         else theRole =result.account.role ;
       }
     })
  }
  return { role:theRole,error:theError} 
} 

async function register(thePlayerName,thePassword) 
// (OUT) error
{
  var theAccount = null ;
  var theError   = "" ;
  await getAccount(thePlayerName).then((result) =>{ theError = result.error ; theAccount=result.item==null?null : result.Account ; });
  if (theAccount!=null) theError = "Compte déjà existant" ;
  else await createAccount(thePlayerName,thePassword).then((result) =>{ theError = result });
  return theError ;
}

async function setRights(thePlayerName,role) 
// (OUT) error         // role="A"(Admin) / "C"(utilisateur lambda connecté) /"" (supprimer le compte)
{
  var theError       = "" ;
  var filter         = { playerName : thePlayerName } ;
  var account        = { } ;
  // vérifier si le compte associé a playerName existe
  await getAccount(thePlayerName).then((result) =>{ account = result.account ; theError = result.error});
  if (account==null) theError = "Utilisateur '"+thePlayerName+"'inconnu" ; 
  else if (theError=="") 
  { // modifier les droits ou supprimer le compte
    if (role=="")
      { // supprimer le compte
        await mon.mongoDb_deleteOne('usersCollection',filter,false)
        .then ((result) =>{ theError = result.error ; }) 
        .catch(theError = "problème d'accès aux comptes utilisateurs dans MongoDb : compte non supprimé pour "+thePlayerName) ;
      }
    else
      { // modifier le role  
        account.role=role ;
        await mon.mongoDb_update('usersCollection',filter,account,false)
        .then ((result) =>{ theError = result.error ; }) 
        .catch(theError = "problème d'accès aux comptes utilisateurs dans MongoDb : compte non modifié pour "+thePlayerName) ;
      }
  }
  else theError =  "Il n'y a pas de compte pour "+thePlayerName ; 
  return theError ; 
}

async function createAccount(thePlayerName,thePassword)
// (OUT) error  
{
  var theError       = "" ;
  var theAccount     = { playerName : thePlayerName, password:thePassword, role:"C" } ;
  await getAccount(thePlayerName).then((result)=> {if (result.account!=null) theError = "Utilisateur " + thePlayerName + " déjà existant"})
  if (theError == "") await mon.mongoDb_insertOne('usersCollection',theAccount,false)
                      .then ((result) =>{ theError = result.error ; })
                      .catch(theError = "problème d'écriture d'un nouveau compte utilisateur dans MongoDb") ;
  return theError ;
}

async function getAccount(thePlayerName)
// (OUT) account, error  // account = null si erreur ou non trouvé
{
  var theError       = "" ;
  var filter         = { playerName : thePlayerName } ;
  var theAccount     = { playerName : thePlayerName, password:"",role:"N" } ;
  // vérifier si le compte associé a playerName existe
  await mon.mongoDb_findOne('usersCollection',filter,false)
  .then ((result) =>{ 
    theError   = result.error ; 
    theAccount = result.item ; 
  })
  .catch(theError = "problème d'accès aux comptes utilisateurs dans MongoDb") ;
  return { account:theAccount, error:theError} 
}
 