///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Module de gestion des parties (création et mise à jour des informations d'une partie)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

'use strict';

module.exports = {allGames, getGames, getNewWord, createGame, startGame, stopRound, stopGame, newImage, propose, leaveGame, joinGame, exitGames, getGamesHistory, deleteGameHistory,  }

var mon                 = require('./server_mongo')   ;
var wor                 = require('./server_words')   ;
var rea                 = require('./server_realtime');

// variables spécifiques au serveur Pictionary
var games               = [] ;
var nextGameId          = 1  ;

//====================================================================================================================
//================================ Gestion des parties en attentes et des parties actives =====================================
// structure de type "game" :
//    gameId        <identifiant de la partie>
//    creatorId     <createur de la partie>
//    creatorName   <createur de la partie>
//    dateTime      <date et heure de création>
//    started       <True(partie active)/false(partie en attente)
//    terminated    <True(partie terminée ou annulée/False(partie en attente ou active)>
//    words         <liste des mots à dessiner/deviner, words.length = roundsCount>
//    finders       <list de structures de type "player" ayant trouvé le mot, finders.length = roundsCount>
//    roundsCount   <nombre de manches>
//    roundIndex    <index de la manche en cours>
//    players[]     <list de structures de type "player"> 
//    dialogs       <chaine multiligne indiquant en logne 1 le résutat du tour précédent, puis sur chaque ligne nom de joueur et événement(proposition, solution, fin de manche ou de partie)
//    dialog0       résutat du tour précédent(Manche i/n : le mot <mot> a été trouvé par <joueur> / Manche n terminée: le mot <mot> n'a été trouvé par personne)
//    _id           identifiant MongoDb des parties terminées enregistrées dans MongoDB
// structure de type "player" :
//    id            <identifiant du joueur>
//    name          <nom du joueur>
//    score         <Score dans la partie>
//    gone          <true if player left the game>

// ---------------- outils -------------------

function getGames() 
// (OUT) games[]  
{
  var result = [] ;
  for (var i=0;i<games.length;i++) result.push(gameToReducedGame(games[i])) ;
  return result ;
}

function getGames_by_Id(gameId) 
// (OUT) game  
{
  var result = null ;
  for (var i=0;i<games.length;i++) if (games[i].gameId==gameId)  result = games[i] ;
  return result ;
}

function getGamePlayer_By_Id(game,playerId)
{
    var result = null ;
    for (var i=0;i<game.players.length;i++) if (game.players[i].id==playerId)  result = game.players[i] ;
    return result ;
}

function gameToReducedGame(game) 
// (OUT) reducedgame  
{ 
  var result = JSON.parse(JSON.stringify(game)) ;
  result.words  = [] ;
  result.finders= [] ;
  for (var i=0;i<=game.roundIndex;i++) { result.words.push(game.words[i]) ; result.finders.push(game.finders[i]) ; }
  return result ; 
}

function gameToRecordedGame(game) 
// (OUT) recordedgame  
{ 
  var result = JSON.parse(JSON.stringify(game)) ;
  delete result.dialogs ;
  delete result.dialog0 ;
  return result ; 
}

function currentGames(theGames)
// (OUT) currentgames[]
{
  var result=[] ;
  for (var i=0;i<theGames.length;i++) if (!theGames[i].terminated) result.push(theGames[i]) ;
  return result ; 
}

function setGameAndGamesRefreshActions(game,theGames)
//(OUT) null
{ 
  var theCurrentGames = theGames==null ? null : currentGames(theGames) ;
  rea.RefreshAction_UpdateItem(-1,null,game,theCurrentGames) ;
}

function processGameEnd(game)
{
  game.terminated=true ;
  for (var i=games.length-1;i>=0;i--) if (games[i].gameId==game.gameId) games.splice(i, 1) ;
  if (game.started) mon.mongoDb_insertOne("gamesCollection", gameToRecordedGame(game), false); 
  setGameAndGamesRefreshActions(game,games) ;
}

// ---------------------------- fonctions appelées par app.post/get/put -----------------------------

// getNewWord (IN) gameId (OUT) word,error 
function getNewWord(gameId)
{
    var game             = getGames_by_Id(gameId) ;
    game.newWordRequired = false ;
    var result           = game==null?{word: "", error: "partie non trouvée"}:{word: game.words[game.roundIndex], error:""}
    return result
}

async function createGame(playerId,playerName,theRoundsCount) 
// (OUT) {game, word, error} ! procedure appelée par le créateur
{
    var theWords   = [] ;
    var theError   = "" ;
    await wor.getRandomWords(theRoundsCount).then ((result) =>{ theError = result.error ; theWords = result.words })
    var theFinders = [] ; for (var i=0;i<theWords.length;i++) theFinders.push(null) ;
    var theGame    = { gameId:nextGameId++, creatorId:playerId, creatorName:playerName,words:theWords,finders:theFinders,roundsCount:theRoundsCount, roundIndex:0, 
                       dateTime: new Date(), started:false, terminated:false, players:[], dialogs:"",dialog0:"", newWordRequired : false} ;
    games.push(theGame) ;
    rea.RefreshAction_UpdateItem(-1,null,null,games) // (OUT) null 
    return {game:theGame, word : theGame.words[0], error:theError}
}

function startGame(gameId) 
//(OUT) { word,error } ! procedure appelée par le créateur
{
    var game   = getGames_by_Id(gameId) ;
    var result = {word : "", error : ""};
    if (game == null)      result.error   = "jeu non trouvé" ;
    else if (game.started) result.error   = "partie déjà démarrée" ;
    else { result.word = game.words[0]; game.started = true; setGameAndGamesRefreshActions(game,games) ; }
    return result ;
}

function stopRound(gameId,terminate) 
// (OUT) { word,error } ! procedure appelée par le créateur
{
    var game     = getGames_by_Id(gameId) ;
    var theError = "" ;
    var theWord  = ""  ;
    if (game == null)                     theError   = "jeu non trouvé" ;
    else if (!game.started && !terminate) theError   = "partie en attente" ;
    else
    {
       game.dialog0     =  "Manche "+(game.roundIndex+1)+"/"+game.roundsCount+" terminée : le mot "+game.words[game.roundIndex]+" n'a été trouvé par personne\n" ;
       game.dialogs     =  game.dialog0+"----------------------------------------\n"
       game.roundIndex++ ; 
       if (game.roundIndex>=game.roundsCount || terminate) processGameEnd(game) ;
       else 
       {
         setGameAndGamesRefreshActions(game,null) ; 
         theWord = game.words[game.roundIndex] ;
       }
    }
    return { word:theWord, error:theError} ;
}

function stopGame(gameId)  
// (OUT) error procedure appelée par le créateur
{
  var theGame         = getGames_by_Id(gameId) ;
  var stopRoundResult = theGame==null?{error : ""} : stopRound(gameId,true) ;
  return (stopRoundResult.error) ;
}

function newImage(gameId, imageData) 
// (OUT) error ! procedure appelée par le créateur
{
    var game   = getGames_by_Id(gameId) ;
    var result = "" ;
    if (game == null)       result   = "jeu non trouvé" ;
    else if (!game.started) result   = "partie en attente" ;
    else for (var i=0 ; i<game.players.length; i++) if (!game.players[i].gone) rea.RefreshAction_UpdateItem(game.players[i].id, imageData, null, null, null, null) ;
    return result ;
}

function propose(gameId,playerId,word) 
// (OUT) { game,success,error } ! procedure appelée par un joueur
{  
   var theGame      = getGames_by_Id(gameId) ;
   var player       = theGame==null?null:getGamePlayer_By_Id(theGame,playerId) ;
   var theSuccess   = false ;
   if (player!=null)
   {
       if (word==theGame.words[theGame.roundIndex]) 
       {
         theSuccess          = true ;
         player.score       += 5 ;
         theGame.dialog0     = "Manche "+(theGame.roundIndex+1)+"/"+theGame.roundsCount+" terminée : le mot "+theGame.words[theGame.roundIndex]+" a été trouvé par "+player.name+"\n" ;
         theGame.dialogs     =  theGame.dialog0+"----------------------------------------\n"
         theGame.finders[theGame.roundIndex] = player ;
         theGame.roundIndex ++ ; 
         theGame.terminated = theGame.roundIndex>=theGame.roundsCount ;
         theGame.newWordRequired = true ; 
       }
       else { player.score-=1 ; theGame.dialogs+=player.name+" : "+word+" <ce n'est pas la solution>\n" ; }
       setGameAndGamesRefreshActions(theGame,null) ; 
    }
    return { game:theGame, success:theSuccess, error:"" } ;
}

function leaveGame(gameId,playerId) 
// (OUT) null ! procedure appelée par un joueur 
{
    var theGame      = getGames_by_Id(gameId) ;
    var player       = theGame==null?null:getGamePlayer_By_Id(theGame,playerId) ;
    if (player!=null && !theGame.started)
    {
      for (var i=theGame.players.length-1;i>=0;i--) if (theGame.players[i].id==playerId) theGame.players.splice(i, 1) ;
      setGameAndGamesRefreshActions(theGame,null) ; 
    }
    else if (player!=null && !theGame.terminated) { player.gone=true ; setGameAndGamesRefreshActions(theGame,null) ; }
 }
 
 function joinGame(gameId, playerId, playerName) 
 // (OUT) { game,error } ! procedure appelée par un joueur
 {
   var theGame  = getGames_by_Id(gameId) ;
   var theError = "" ;
   if (theGame==null || theGame.started) { theGame == null  ; theError = "Partie déjà démarrée ou annulée" } 
   else 
   {
     for (var i=0 ; i<theGame.players.length && playerId >= 0 ; i++) if (theGame.players[i].id == playerId) playerId = -1 ;
     if (playerId != -1)
     {
       theGame.players.push({id : playerId, name : playerName, score : 0, gone : false}) ;
       setGameAndGamesRefreshActions(theGame,null) ;
     }
   }
   return { game:theGame, error:theError }
 }
 
 function exitGames(playerId) 
 // (OUT) null
 {
   for (var i=0;i<games.length;i++) 
   {
    for (var j=0;j<games[i].players.length;j++) if (games[i].players[j].id==playerId) leaveGame(games[i].gameId,games[i].players[j].id) ;
    if (games[i].creatorId==playerId) { stopGame(games[i].gameId) ; i=games.length+1; }
   }
 }
 

// -- Gestion des parties terminées --

async function getGamesHistory(playerName) //  (OUT) games[],error  <playerName="" pour tous les joueurs> < games : array of game >
{
  var theError = "";
  var theItems = [];
  await mon.mongoDb_findItems("gamesCollection", (playerName == "")?{}:{creatorName : playerName})
  .then ((result) =>{ 
    theError   = result.error ; 
    theItems = result.items ; 
  })
  .catch(theError = "problème d'accès à l'historique des parties dans MongoDb") ;
  return {games : theItems, error: theError}
}

async function deleteGameHistory(dbGameId) // (OUT) error    <dbGameId=-1 pour toutes les parties>
{
  var error = "";
  if (dbGameId<0) await mon.mongoDb_deleteMany('gamesCollection', {}                        ).then ((res) =>{error = res.error}) ;
  else            await mon.mongoDb_deleteOne ('gamesCollection', {_id : mon.mongoDb_getObjectId(dbGameId)}).then ((res) =>{error = res.error}) ;
  return error
}

function allGames() {return games}