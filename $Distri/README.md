# Projet Pictionary DTY, William BINQUET

Ceci est un projet d'application web du jeu Pictionary dans le cadre d'une évaluation d'aptitude pour le parcours DTY.  <br/>
(Le git contenant encore des fichiers me servant pour le développement, le contenu du repository est temporaire et sera mis à jour pour le rendu final)

## Lien

https://gitlab.com/William_BINQUET/pictionary_dty.git

## Documentation 

La documentation complète est fournie dans le fichier DTY.pdf. Certaines parties ne sont pas à jour et seront modifiées pour le rendu final.

## Installation 

Il est recommandé de cloner le git à l'intérieur d'un dossier <Appli_root> (par exemple **C:\DTY**) via la commande :

```bash
git clone https://gitlab.com/William_BINQUET/pictionary_dty.git
```

Il est conseillé d'installer des différents programmes nécessaires au bon fonctionnement de l'application dans le dossier <Appli_root>. 

### Node.js

Télécharger **"Node.js source code"** depuis l'URL https://nodejs.org/en/download/ <br/>
Lancer le .msi téléchargé en choisissant comme répertoire d'installation <Appli_root>

### SGBD Mongodb

Télécharger **Mongodb** depuis l'URL Naviguer vers l'URL https://www.mongodb.com/download-center/community  <br/>
Lancer le .msi et utiliser toutes les options ou valeurs par défaut sauf : <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; dans le panneau Service configuration où l'on indiquera <Appli_root>\MongoDB\data\ et <Appli_root>\MongoDB\data\log\ comme Data directory et Log directory. <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; dans le panneau Install MongoDB Compass où l'on décochera l'option d'installation de MongoDB Compass. (en laissant cette option activée, le chargement peut être bloqué lors de l'installation). <br/>

Télécharger **Mongodb Compass** depuis https://www.mongodb.com/download-center/compass le .msi de MongoDB Compass, et l'exécuter. <br/>
(sur le premier écran, indiquer <Appli_root>\MongoDB Compass\ comme répertoire de destination) <br/>

### Google Chrome (Navigateur, pas nécessaire)

Télécharger **Chrome** depuis https://www.google.com/intl/fr_fr/chrome/ et lancer l'installation à partir du fichier .exe reçu.

## Téléchargement des packages

L'installation des modules utiles se fait à l'aide d'un fichier BATCH : **node_requierements_install.bat** <br/> <br/>
Pour executer ce BATCH, on peut   : <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; soit double-cliquer sur le fichier <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; soit lancer l'invite de cmd. Dans l'invite de commande se positionner sur le répertoire de l'application (par exemple : <Appli_root>\pictonnary_dty) et executer la commande suivante :   <br/>

```bash
start node_requirements_install.bat
```

## Lancement de l'application WEB 

### Lancement du server

On lance le server en utilisant le fichier BATCH **"start_server.bat"** du répertoire. <br/> 
Pour executer ce BATCH, on peut   : <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; soit double-cliquer sur le fichier <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; soit lancer l'invite de cmd. Dans l'invite de commande se positionner sur le répertoire de l'application et lancer le server via la commande : <br/>
```bash
start node server.js 
```

### Lancement du client

Après le lancement du server, le client s'execute en se connectant avec un navigateur Web (par exemple Google Chrome) au port 3000 du server, soit en local : <br/>
http://localhost:3000/ 

