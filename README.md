# *Projet Pictionary DTY, William BINQUET*

Ceci est un projet d'application web du jeu Pictionary dans le cadre d'une évaluation d'aptitude pour le parcours DTY.  <br/>

## 1. Lien 

https://gitlab.com/William_BINQUET/pictionary_dty.git

## 2. Desctiption des fonctionnalités utilisateurs

L'application inclut une gestion des droits : un utilisateur peut être administrateur ou non. 
Ce projet permet à l'utilisateur de : 
* créer une partie ou visualiser une liste de parties qu’il peut rejoindre.
* en tant que créateur, l'utilisateur peut dessiner (click gauche) et effacer (click droit) un dessin qu'il doit faire deviner aux participants. 
* en tant que participant, l'utilisateur peut deviner un dessin et soumettre sa proposition. (La communication entre tous les participants d'une partie se fait en temps réel)
* s’authentifier et de visualiser son historique des parties crées.
* si il est administrateur, l'utilisateur a accès à une vision de l'historique de toutes les parties et la possibilité de les supprimer de la base de données. 
* si il est administrateur, l'utilisateur a accès aussi à une page qui permet de modifier un référentiel de mots stockés en base, en ajoutant des mots ou supprimant des anciens.
* si il est administrateur, l'utilisateur peut donner ou supprimer les droits des autres comptes.

## 3. Documentation 

La documentation complète est fournie dans le fichier **DTY_v3.pdf**.

## 4. Installation des pré-requis

Il est recommandé de cloner le git à l'intérieur d'un dossier <Appli_root> (par exemple **C:\DTY**) via la commande :

```bash
git clone https://gitlab.com/William_BINQUET/pictionary_dty.git
```

Il est conseillé d'installer les différents programmes nécessaires au bon fonctionnement de l'application en utilisant, sauf mention contraire, les paramètres d'installation par défaut.

### NodeJs

Télécharger **"Node.js source code"** depuis l'URL https://nodejs.org/en/download/ la version **node-v12.14.0** <br/>
Lancer le .msi téléchargé.

### SGBD Mongodb

Télécharger **Mongodb** depuis l'URL Naviguer vers l'URL https://www.mongodb.com/download-center/community  (OS : Windows x64 x64, Package : MSI, **Version : 4.2.2**) <br/>
Lancer le .msi et utiliser toutes les options ou valeurs par défaut sauf : <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; dans le panneau Install MongoDB Compass où l'on décochera l'option d'installation de MongoDB Compass (avec cette option activée, le chargement peut être bloqué lors de l'installation). <br/>

Télécharger **Mongodb Compass** depuis https://www.mongodb.com/download-center/compass le .msi de MongoDB Compass, et l'exécuter. <br/>

### Google Chrome (Navigateur, installation pas nécessaire)

Télécharger **Chrome** depuis https://www.google.com/intl/fr_fr/chrome/ et lancer l'installation à partir du fichier .exe reçu.

### Installation de Visual Studio Code (développement seulement)
Télécharger Visual Studio Code depuis https://code.visualstudio.com/ et choisir la **version 1.4.1.1**. Exécuter le  fichier «.exe » téléchargé avec les options par défaut. <br/>

Cliquez sur l’icone « Extensions » du panneau de gauche :
* choisir parmi les extensions « Popular » le module « Debugger for Chrome » et cliquer sur le bouton « install ».
* choisir parmi les extensions « Popular » le module « ESLint » et cliquer sur le bouton « install ».

## 5. Deploiement de l'application et environnement de developpement

Pour l'évaluation, j'ai décidé de séparer le rendu en deux dossiers : "$Distri" et "$Dev". <br/>

### Déploiement de l'application
Le dossier "$Distri" est le dossier du deploiement de l'application. Après installation des pré-requis :
* Ouvrir https://gitlab.com/William_BINQUET/pictonnary_dty sur gitlab et récupérer le "repository" (utiliser le bouton de download ou une commande git clone).
* Pour installer les node_modules de l'environnement de production, exécuter la commande :
```bash
npm install
```
* Pour lancer le server, exécuter la commande :
```bash
node server.js
```
* Pour utiliser un autre port que celui par défaut (3000) comme par exemple 7000, lancer la commande :
```bash
set PORT=7000 && node server.js
```
* Répondre au "prompt" demandant si l'on utilise "mongoose" et/ou "socket.io".
* Pour lancer un client, lancer un navigateur ouvrant une page sur le port du serveur, par exemple http://localhost3000/ pour le port 3000.

> NOTE : La base de données et les collections de MongoDb sont créées automatiquement au premier lancement du serveur. <br/> 
Avec un référentiel de mots vide que l'administrateur doit alimenter via l'option du menu "Référentiel de mots"

### Mise en place de l'environnement de développement
Le dossier "$Dev" contient les sources du back-end et du front-end (les fichiers *.tsx, *.js, *.scss, *.html) ainsi que la configuration de développement (les fichiers *.json, *.bat). Après installation des pré-requis :
* Extraire le contenu du fichier Dev.zip dans un nouveau répertoire de developpement, noté <Pic_Dev> dans la suite de ce chapitre.
* Pour installer les node_modules de l'environnement de développement, exécuter la commande : 
```bash
npm install
```
* Lancer Visual Studio code.
* Ouvrir le dossier <Pic_Dev>
* Ouvrir l'onglet de debug 
* Dans "Debug and run", choisir l'option "debug Chrome" et lancer le debug,

## 6. Tests

### Tests en local 

Il est recommandé d'utiliser Google Chrome. 

Si on se sert de Firefox, on évitera de lancer plusieurs clients sur la même machine, sauf si on utilise des nouvelles fenêtres de navigation privée ou si on lance le serveur avec une configuration sans socket.io.
(Ce problème est dû à une particularité de Firefox. Si on se connecte au serveur avec la même url dans un nouvel onglet, le navigateur bascule sur le premier onglet)  

### Test fonctionnalité d'Admin

Pour se connecter en qualité d'administrateur, sélectionner l'option **"Me connecter"** du menu et rentrer : <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; l'identifiant   : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**admin#secours** <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; le mot de passe : **admin#password**

On pourra initialiser le référentiel de mots en choisissant le menu **"Référentiel de mots"** et en faisant un copier-coller du contenu du fichier "mots.txt", <br/>
puis en cliquant sur le bouton "Valider".

